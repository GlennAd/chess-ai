#include "Genome.h"
#include "Config.h"
#include <fstream>
#include <iostream>
#if defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#include <Windows.h>
#endif

Genome::Genome(int rows, int columns, int inputNodes, int outputNodes)
{
	genes.reserve(rows * columns);

	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < columns; ++j)
		{
			Gene* gene = new Gene(i, j);
			genes.push_back(gene);
		}
	}

	std::vector<Gene*>::iterator geneIterator;
	std::vector<Gene*>::iterator secondGeneIterator;
	std::vector<Node*>::iterator nodeIterator;
	std::vector<Node*>::iterator secondNodeIterator;

	for (int i = 0; i < inputNodes; ++i)
	{
		//Node* node = new Node(0, sf::Vector2f(0, 200 + i * lengthBetweenNodes * 2));
		Node* node = new Node(0);
		this->inputNodes.push_back(node);
	}

	for (int i = 0; i < outputNodes; ++i)
	{
		//Node* node = new Node(0, sf::Vector2f(0 + columns * lengthBetweenGenes, 200 + i * lengthBetweenNodes * 2));
		Node* node = new Node(0);
		this->outputNodes.push_back(node);
	}

	for (int i = 0; i < inputNodes; ++i)
	{
		makeRandomOutLink(this->inputNodes[i]);
	}

	for (int i = 0; i < outputNodes; ++i)
	{
		makeRandomInLink(this->outputNodes[i]);
	}

	for (geneIterator = genes.begin(); geneIterator != genes.end(); ++geneIterator)
	{
		for (nodeIterator = (*geneIterator)->inputNodes.begin(); nodeIterator != (*geneIterator)->inputNodes.end(); ++nodeIterator)
		{
			makeRandomInLink((*nodeIterator));
		}

		for (nodeIterator = (*geneIterator)->outputNodes.begin(); nodeIterator != (*geneIterator)->outputNodes.end(); ++nodeIterator)
		{
			makeRandomOutLink((*nodeIterator));
		}
	}

	genomeLength = columns;
}

Genome::Genome(int id)
{
	std::string filePath = "./resources/genomes/genome" + std::to_string(id);

	std::ifstream file(filePath + "/config.txt");

	if (file.is_open())
	{
		int x, y;

		file >> x;
		file >> y;
		
		genomeLength = x;

		for (int i = 0; i < x; ++i)
		{
			for (int j = 0; j < y; ++j)
			{
				Gene* gene = new Gene(filePath + "/gene" + std::to_string(i * genomeLength + j) + ".txt", i, j);

				genes.push_back(gene);
			}
		}

		file >> x;
		file >> y;

		for (int i = 0; i < x; ++i)
		{
			//Node* node = new Node(0, sf::Vector2f(0, 200 + i * lengthBetweenNodes * 2));
			Node* node = new Node(0);
			this->inputNodes.push_back(node);
		}

		for (int i = 0; i < y; ++i)
		{
			//Node* node = new Node(0, sf::Vector2f(0 + genomeLength * lengthBetweenGenes, 200 + i * lengthBetweenNodes * 2));
			Node* node = new Node(0);
			this->outputNodes.push_back(node);
		}

		int links;

		file >> links;

		for (int i = 0; i < links; ++i)
		{
			int inputGene, inputNodeNumber, outputGene, outputNodeNumber;
			float weight;

			file >> inputGene >> inputNodeNumber >> outputGene >> outputNodeNumber >> weight;

			Node* inputNode, *outputNode;

			if (inputGene == -1)
			{
				inputNode = inputNodes[inputNodeNumber];
			}
			else
			{
				inputNode = genes[inputGene]->outputNodes[inputNodeNumber];
			}

			if (outputGene == -1)
			{
				outputNode = outputNodes[outputNodeNumber];
			}
			else
			{
				outputNode = genes[outputGene]->inputNodes[outputNodeNumber];
			}

			geneLinks.push_back(new Link(inputNode, outputNode, weight));
		}

	}
	else
	{
		printf("Genome couldn't open file.\n");
	}
}

#if defined(__linux__)
void Genome::writeToFile(int id)
{
	std::string filePath = "./resources/genomes/genome" + std::to_string(id);
	std::string command = "mkdir -p " + filePath;
	system(command.c_str());

	std::ofstream file(filePath + "/config.txt");
	if (file.is_open())
	{
		file << genomeLength << '\n';
		file << genes.size() / genomeLength << '\n';

		for (size_t i = 0; i < genes.size(); ++i)
		{
			genes[i]->writeToFile(filePath + "/gene" + std::to_string(i) + ".txt");
		}
		file << inputNodes.size() << ' ' << outputNodes.size() << '\n';

		file << geneLinks.size() << '\n';

		for (int i = 0; i < geneLinks.size(); ++i)
		{
			int inputGene, inputNode;
			int outputGene, outputNode;

			for (int j = 0; j < genes.size(); ++j)
			{
				for (int x = 0; x < genes[j]->outputNodes.size(); ++x)
				{
					if (geneLinks[i]->inNode == genes[j]->outputNodes[x])
					{
						inputGene = j;
						inputNode = x;
					}
				}
				for (int x = 0; x < genes[j]->inputNodes.size(); ++x)
				{
					if (geneLinks[i]->outNode == genes[j]->inputNodes[x])
					{
						outputGene = j;
						outputNode = x;
					}
				}
			}

			for (int x = 0; x < inputNodes.size(); ++x)
			{
				if (geneLinks[i]->inNode == inputNodes[x])
				{
					inputGene = -1;
					inputNode = x;
				}
			}

			for (int x = 0; x < outputNodes.size(); ++x)
			{
				if (geneLinks[i]->outNode == outputNodes[x])
				{
					outputGene = -1;
					outputNode = x;
				}
			}
		
			file << inputGene << ' ' << inputNode << ' ' << outputGene << ' ' << outputNode << ' ' << geneLinks[i]->weight << '\n';

		}
	}
}

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
void Genome::writeToFile(int id)
{
	std::string filePath = "./resources/genomes/genome" + std::to_string(id);

	if (CreateDirectoryA(filePath.c_str(), NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
		std::ofstream file(filePath + "/config.txt");
		
		if (file.is_open())
		{
			file << genomeLength << '\n';
			file << genes.size() / genomeLength << '\n';

			for (int i = 0; i < genes.size(); ++i)
			{
				genes[i]->writeToFile(filePath + "/gene" + std::to_string(i) + ".txt");
			}
			
			file << inputNodes.size() << ' ' << outputNodes.size() << '\n';
			

			file << geneLinks.size() << '\n';

			for (int i = 0; i < geneLinks.size(); ++i)
			{
				int inputGene, inputNode;
				int outputGene, outputNode;

				for (int j = 0; j < genes.size(); ++j)
				{
					for (int x = 0; x < genes[j]->outputNodes.size(); ++x)
					{
						if (geneLinks[i]->inNode == genes[j]->outputNodes[x])
						{
							inputGene = j;
							inputNode = x;
						}
					}

					for (int x = 0; x < genes[j]->inputNodes.size(); ++x)
					{
						if (geneLinks[i]->outNode == genes[j]->inputNodes[x])
						{
							outputGene = j;
							outputNode = x;
						}
					}
				}

				for (int x = 0; x < inputNodes.size(); ++x)
				{
					if (geneLinks[i]->inNode == inputNodes[x])
					{
						inputGene = -1;
						inputNode = x;
					}
				}

				for (int x = 0; x < outputNodes.size(); ++x)
				{
					if (geneLinks[i]->outNode == outputNodes[x])
					{
						outputGene = -1;
						outputNode = x;
					}
				}

				file << inputGene << ' ' << inputNode << ' ' << outputGene << ' ' << outputNode << ' ' << geneLinks[i]->weight << '\n';
			}
		}
		else
		{
			printf("Genome couldn't open file for writing.\n");
		}
	}

}
#endif

Genome::Genome(Genome* genome)
{
	genomeLength = genome->genomeLength;

	genes.clear();
	genes.reserve(genome->genes.size());

	for (int i = 0; i < genome->genes.size(); ++i)
	{
		Gene* gene = new Gene(genome->genes[i]);
		genes.push_back(gene);
	}

	//You can never have too many iterators
	//Used to cycle parent
	std::vector<Gene*>::iterator geneIterator;
	//Used to cycle child
	std::vector<Gene*>::iterator secondGeneIterator;
	//Parent
	std::vector<Gene*>::iterator thirdGeneIterator;
	//Child
	std::vector<Gene*>::iterator fourthGeneIterator;

	//Used to cycle parent
	std::vector<Node*>::iterator nodeIterator;
	//Used to cycle child
	std::vector<Node*>::iterator secondNodeIterator;
	//Parent
	std::vector<Node*>::iterator thirdNodeIterator;
	//Child
	std::vector<Node*>::iterator fourthNodeIterator;

	std::vector<Link*>::iterator linkIterator;

	for (int i = 0; i < genome->inputNodes.size(); ++i)
	{
		//Node* node = new Node(0, sf::Vector2f(0, 200 + i * lengthBetweenNodes * 2));
		Node* node = new Node(0);
		this->inputNodes.push_back(node);
	}

	for (int i = 0; i <  genome->outputNodes.size(); ++i)
	{
		//Node* node = new Node(0, sf::Vector2f(0 + genomeLength * lengthBetweenGenes, 200 + i * lengthBetweenNodes * 2));
		Node* node = new Node(0);
		this->outputNodes.push_back(node);
	}

	//Now this is a complicated mess, better keep your focus
	//Cycles origin gene for links. Going through parents genes and the corresponding child gene, transfering gene links to the child
	for (geneIterator = genome->genes.begin(), thirdGeneIterator = genes.begin(); geneIterator != genome->genes.end(); ++geneIterator, ++thirdGeneIterator)
	{
		//Cycles target gene for links
		for (secondGeneIterator = geneIterator, fourthGeneIterator = thirdGeneIterator; secondGeneIterator != genome->genes.end(); ++secondGeneIterator, ++fourthGeneIterator)
		{
			//Origin nodes for parent and the corresponding one for child
			for (nodeIterator = (*geneIterator)->inputNodes.begin(), thirdNodeIterator = (*thirdGeneIterator)->inputNodes.begin();
				nodeIterator != (*geneIterator)->inputNodes.end();
				++nodeIterator, ++thirdNodeIterator)
			{
				//Target node for parent and the corresponding one for child
				for (secondNodeIterator = (*secondGeneIterator)->outputNodes.begin(), fourthNodeIterator = (*fourthGeneIterator)->outputNodes.begin();
					secondNodeIterator != (*secondGeneIterator)->outputNodes.end();
					++secondNodeIterator, ++fourthNodeIterator)
				{
					for (linkIterator = (*nodeIterator)->getInLinks().begin(); linkIterator != (*nodeIterator)->getInLinks().end(); ++linkIterator)
					{
						if ((*linkIterator)->outNode == (*nodeIterator) && (*linkIterator)->inNode == (*secondNodeIterator))
						{
							Link* link = new Link((*fourthNodeIterator), (*thirdNodeIterator), (*linkIterator)->weight);
							geneLinks.push_back(link);
							break;
						}
					}
				}

				for (secondNodeIterator = genome->inputNodes.begin(), fourthNodeIterator = inputNodes.begin();
					secondNodeIterator != genome->inputNodes.end();
					++secondNodeIterator, ++fourthNodeIterator)
				{
					for (linkIterator = (*nodeIterator)->getInLinks().begin(); linkIterator != (*nodeIterator)->getInLinks().end(); ++linkIterator)
					{
						if ((*linkIterator)->outNode == (*nodeIterator) && (*linkIterator)->inNode == (*secondNodeIterator))
						{
							Link* link = new Link((*fourthNodeIterator), (*thirdNodeIterator), (*linkIterator)->weight);
							geneLinks.push_back(link);
							break;
						}
					}
				}
			}



			//Origin nodes for parent and the corresponding one for child
			for (nodeIterator = (*geneIterator)->outputNodes.begin(), thirdNodeIterator = (*thirdGeneIterator)->outputNodes.begin();
				nodeIterator != (*geneIterator)->outputNodes.end();
				++nodeIterator, ++thirdNodeIterator)
			{
				//Target node for parent and the corresponding one for child
				for (secondNodeIterator = (*secondGeneIterator)->inputNodes.begin(), fourthNodeIterator = (*fourthGeneIterator)->inputNodes.begin();
					secondNodeIterator != (*secondGeneIterator)->inputNodes.end();
					++secondNodeIterator, ++fourthNodeIterator)
				{
					for (linkIterator = (*nodeIterator)->getOutLinks().begin(); linkIterator != (*nodeIterator)->getOutLinks().end(); ++linkIterator)
					{
						if ((*linkIterator)->inNode == (*nodeIterator) && (*linkIterator)->outNode == (*secondNodeIterator))
						{
							Link* link = new Link((*thirdNodeIterator), (*fourthNodeIterator), (*linkIterator)->weight);
							geneLinks.push_back(link);
							break;
						}
					}
				}

				for (secondNodeIterator = genome->outputNodes.begin(), fourthNodeIterator = outputNodes.begin();
					secondNodeIterator != genome->outputNodes.end();
					++secondNodeIterator, ++fourthNodeIterator)
				{
					for (linkIterator = (*nodeIterator)->getOutLinks().begin(); linkIterator != (*nodeIterator)->getOutLinks().end(); ++linkIterator)
					{
						if ((*linkIterator)->inNode == (*nodeIterator) && (*linkIterator)->outNode == (*secondNodeIterator))
						{
							Link* link = new Link((*thirdNodeIterator), (*fourthNodeIterator), (*linkIterator)->weight);
							geneLinks.push_back(link);
							break;
						}
					}
				}
			}
		}
	}
	
	//Copies over links from genome input nodes to genome output nodes. They can actually happen
	for (nodeIterator = genome->inputNodes.begin(), thirdNodeIterator = inputNodes.begin(); nodeIterator != genome->inputNodes.end(); ++nodeIterator, ++thirdNodeIterator)
	{
		for (secondNodeIterator = genome->outputNodes.begin(), fourthNodeIterator = outputNodes.begin(); secondNodeIterator != genome->outputNodes.end(); ++secondNodeIterator, ++fourthNodeIterator)
		{
			for (linkIterator = (*nodeIterator)->getOutLinks().begin(); linkIterator != (*nodeIterator)->getOutLinks().end(); ++linkIterator)
			{
				if ((*linkIterator)->inNode == (*nodeIterator) && (*linkIterator)->outNode == (*secondNodeIterator))
				{
					Link* link = new Link((*thirdNodeIterator), (*fourthNodeIterator), (*linkIterator)->weight);
					geneLinks.push_back(link);
					break;
				}
			}
		}
	}
}

Genome::~Genome()
{
	std::vector<Node*>::iterator nodeIterator;
	std::vector<Gene*>::iterator geneIterator;

	for (nodeIterator = outputNodes.begin(); nodeIterator != outputNodes.end(); ++nodeIterator)
	{
		delete (*nodeIterator);
	}

	for (nodeIterator = inputNodes.begin(); nodeIterator != inputNodes.end(); ++nodeIterator)
	{
		delete (*nodeIterator);
	}

	for (geneIterator = genes.begin(); geneIterator != genes.end(); ++geneIterator)
	{
		delete (*geneIterator);
	}
}

void Genome::mutate()
{
	std::vector<Gene*>::iterator geneIterator;
	std::vector<Gene*>::iterator secondGeneIterator;
	std::vector<Node*>::iterator nodeIterator;
	std::vector<Node*>::iterator secondNodeIterator;

	std::vector<Link*>::iterator linkIterator;
	std::vector<Link*>::iterator secondLinkIterator;

	for (linkIterator = geneLinks.begin(); linkIterator != geneLinks.end();)
	{
		if (percentageDistributor(randomGen) < deleteLinkMutation)
		{
			if ((*linkIterator)->inNode->getOutLinks().size() > 1 && (*linkIterator)->outNode->getInLinks().size() > 1)
			{
				for (secondLinkIterator = (*linkIterator)->inNode->getOutLinks().begin(); secondLinkIterator != (*linkIterator)->inNode->getOutLinks().end(); ++secondLinkIterator)
				{
					if ((*secondLinkIterator) == (*linkIterator))
					{
						(*linkIterator)->inNode->getOutLinks().erase(secondLinkIterator);
						break;
					}
				}

				for (secondLinkIterator = (*linkIterator)->outNode->getInLinks().begin(); secondLinkIterator != (*linkIterator)->outNode->getInLinks().end(); ++secondLinkIterator)
				{
					if ((*secondLinkIterator) == (*linkIterator))
					{
						(*linkIterator)->outNode->getInLinks().erase(secondLinkIterator);
						break;
					}
				}

				delete (*linkIterator);
				linkIterator = geneLinks.erase(linkIterator);
				
			}
			else
			{
				++linkIterator;
			}
		}
		else
		{
			++linkIterator;
		}
	}

	if (percentageDistributor(randomGen) > newLinkMutation)
	{
		std::uniform_int_distribution<int> genePicker(0, genes.size() - 1);
		int genePicked = genePicker(randomGen);

		if (coinflipDistributor(randomGen) == 0)
		{
			std::uniform_int_distribution<int> nodePicker(0, genes[genePicked]->inputNodes.size() - 1);

			makeRandomInLink(genes[genePicked]->inputNodes[nodePicker(randomGen)]);
		}
		else
		{
			std::uniform_int_distribution<int> nodePicker(0, genes[genePicked]->outputNodes.size() - 1);

			makeRandomOutLink(genes[genePicked]->outputNodes[nodePicker(randomGen)]);
		}
	}

	for (linkIterator = geneLinks.begin(); linkIterator != geneLinks.end(); ++linkIterator)
	{
		if (percentageDistributor(randomGen) < weightChangeMutation)
		{
			(*linkIterator)->mutate();
		}
	}

	int geneMutateValue;

	for (geneIterator = genes.begin(); geneIterator != genes.end(); ++geneIterator)
	{
		geneMutateValue = (*geneIterator)->mutate();

		if (geneMutateValue > 1)
		{
			geneLinks.clear();
			
			for (secondGeneIterator = genes.begin(); secondGeneIterator != genes.end(); ++secondGeneIterator)
			{
				for (nodeIterator = (*secondGeneIterator)->inputNodes.begin(); nodeIterator != (*secondGeneIterator)->inputNodes.end(); ++nodeIterator)
				{
					geneLinks.insert(geneLinks.end(), (*nodeIterator)->getInLinks().begin(), (*nodeIterator)->getInLinks().end());
				}
			}

			for(nodeIterator = outputNodes.begin(); nodeIterator != outputNodes.end(); ++nodeIterator)
			{
				geneLinks.insert(geneLinks.end(), (*nodeIterator)->getInLinks().begin(), (*nodeIterator)->getInLinks().end());
			}

			//for (nodeIterator = inputNodes.begin(); nodeIterator != inputNodes.end(); ++nodeIterator)
			//{
			//	for (secondNodeIterator = outputNodes.begin(); secondNodeIterator != outputNodes.end(); ++secondNodeIterator)
			//	{
			//		for (linkIterator = (*nodeIterator)->getOutLinks().begin(); linkIterator != (*nodeIterator)->getOutLinks().end(); ++linkIterator)
			//		{
			//			if ((*linkIterator)->outNode == (*secondNodeIterator))
			//			{
			//				geneLinks.push_back((*linkIterator));
			//			}
			//		}
			//	}
			//}
		}
		
		if (geneMutateValue == 1 || geneMutateValue == 3)
		{
			//std::vector<Link*>::iterator linkIterator;


			for (nodeIterator = (*geneIterator)->inputNodes.begin(); nodeIterator != (*geneIterator)->inputNodes.end(); ++nodeIterator)
			{
				if ((*nodeIterator)->getInLinks().size() == 0)
				{
					makeRandomInLink((*nodeIterator));
				}
			}

			for (nodeIterator = (*geneIterator)->outputNodes.begin(); nodeIterator != (*geneIterator)->outputNodes.end(); ++nodeIterator)
			{
				if ((*nodeIterator)->getOutLinks().size() == 0)
				{
					makeRandomOutLink((*nodeIterator));
				}
			}
		}

	}
}

void Genome::input(float input[], float output[])
{
	std::vector<Node*>::iterator nodeIterator;
	std::vector<Link*>::iterator linkIterator;
	std::vector<Gene*>::iterator geneIterator;
	int i;

	for (nodeIterator = inputNodes.begin(), i = 0; nodeIterator != inputNodes.end(); ++nodeIterator, ++i)
	{
		for (linkIterator = (*nodeIterator)->getOutLinks().begin(); linkIterator != (*nodeIterator)->getOutLinks().end(); ++linkIterator)
		{
			(*linkIterator)->value = (*linkIterator)->weight * input[i];
		}
	}

	for (int j = 0; j < genomeLength; ++j)
	{
		for (geneIterator = genes.begin(); geneIterator != genes.end(); ++geneIterator)
		{
			if ((*geneIterator)->column == j)
			{
				(*geneIterator)->activateGene();
			}
		}
	}

	for (nodeIterator = outputNodes.begin(), i = 0; nodeIterator != outputNodes.end(); ++nodeIterator, ++i)
	{
		output[i] = (*nodeIterator)->processOutputValue();
	}
}


/*void Genome::draw(sf::RenderWindow & window)
{
	std::vector<Gene*>::iterator geneIterator;
	std::vector<Node*>::iterator nodeIterator;
	std::vector<Link*>::iterator linkIterator;

	for (linkIterator = geneLinks.begin(); linkIterator != geneLinks.end(); ++linkIterator)
	{
		(*linkIterator)->draw(window);
	}

	for (geneIterator = genes.begin(); geneIterator != genes.end(); ++geneIterator)
	{
		(*geneIterator)->draw(window);
	}

	for (nodeIterator = inputNodes.begin(); nodeIterator != inputNodes.end(); ++nodeIterator)
	{
		(*nodeIterator)->draw(window);
	}

	for (nodeIterator = outputNodes.begin(); nodeIterator != outputNodes.end(); ++nodeIterator)
	{
		(*nodeIterator)->draw(window);
	}
}*/

void Genome::makeRandomOutLink(Node * node)
{
	if (node->getOutLinks().size() < maxLinksOutOfNode)
	{
		std::vector<Gene*>::iterator currGene;
		std::vector<Node*>::iterator currNode;
		std::vector<Link*>::iterator currLink;
		std::vector<Node*> possibleNodes;


		for (currGene = genes.begin(); currGene != genes.end(); ++currGene)
		{
			for (currNode = (*currGene)->inputNodes.begin(); currNode != (*currGene)->inputNodes.end(); ++currNode)
			{
				for (currLink = (*currNode)->getInLinks().begin(); currLink != (*currNode)->getInLinks().end(); ++currLink)
				{
					if ((*currLink)->inNode == node)
					{
						break;
					}
				}

				if (currLink == (*currNode)->getInLinks().end())
				{
					possibleNodes.push_back((*currNode));
				}
			}
		}

		for (currNode = outputNodes.begin(); currNode != outputNodes.end(); ++currNode)
		{
			for (currLink = (*currNode)->getInLinks().begin(); currLink != (*currNode)->getInLinks().end(); ++currLink)
			{
				if ((*currLink)->inNode == node)
				{
					break;
				}
			}

			if (currLink == (*currNode)->getInLinks().end())
			{
				possibleNodes.push_back((*currNode));
			}
		}
		
		if (possibleNodes.size() != 0)
		{
			std::uniform_int_distribution<int> nodePicker(0, possibleNodes.size() - 1);

			Link* link = new Link(node, possibleNodes[nodePicker(randomGen)]);
			geneLinks.push_back(link);
		}
	}
	else
	{
		//printf("Node already has 4+ links out.\n");
	}
}

void Genome::makeRandomInLink(Node * node)
{
	std::vector<Gene*>::iterator currGene;
	std::vector<Node*>::iterator currNode;
	std::vector<Link*>::iterator currLink;
	std::vector<Node*> possibleNodes;


	for (currGene = genes.begin(); currGene != genes.end(); ++currGene)
	{
		for (currNode = (*currGene)->outputNodes.begin(); currNode != (*currGene)->outputNodes.end(); ++currNode)
		{
			for (currLink = (*currNode)->getOutLinks().begin(); currLink != (*currNode)->getOutLinks().end(); ++currLink)
			{
				if ((*currLink)->outNode == node)
				{
					break;
				}
			}

			if (currLink == (*currNode)->getOutLinks().end())
			{
				possibleNodes.push_back((*currNode));
			}
		}
	}

	for (currNode = inputNodes.begin(); currNode != inputNodes.end(); ++currNode)
	{
		for (currLink = (*currNode)->getOutLinks().begin(); currLink != (*currNode)->getOutLinks().end(); ++currLink)
		{
			if ((*currLink)->outNode == node)
			{
				break;
			}
		}

		if (currLink == (*currNode)->getOutLinks().end())
		{
			possibleNodes.push_back((*currNode));
		}
	}

	if (possibleNodes.size() != 0)
	{
		std::uniform_int_distribution<int> nodePicker(0, possibleNodes.size() - 1);

		Link* link = new Link(possibleNodes[nodePicker(randomGen)], node);
		geneLinks.push_back(link);
	}
}
