#pragma once
#include "Camera.h"

#if defined(__linux__)
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#include <SDL2\SDL.h>
#include "glm\glm.hpp"
#endif

class RayCasting
{
public:
	RayCasting(Camera* camera,int screenWidth, int screenHight);
	~RayCasting();
	glm::vec3 mouseClick();
private:
	Camera* camera;
	int screenWidth;
	int screenHight;
};

