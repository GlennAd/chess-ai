#pragma once

#include <vector>
#include <string>

#include "Node.h"
#include "Link.h"

//#include <SFML\Graphics.hpp>
#include <fstream>

class Gene
{
public:
	Gene(int row, int column);
	Gene(std::string file, int row, int column);
	Gene(Gene* gene);
	~Gene();

	void writeToFile(std::string filePath);

	int mutate();

	void activateGene();

	//void draw(sf::RenderWindow& window);

	std::vector<Node*> inputNodes;
	std::vector<Node*> outputNodes;
	
	int column;
private:
	void makeRandomOutLink(Node* node);
	void makeRandomInLink(Node* node);

	std::vector<Node*> nodes;
	std::vector<Link*> links;
	

	//sf::Vector2f offset;

	int generation = 0;
	int geneLength;
};

