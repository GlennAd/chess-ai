#include "MoveBlock.h"

MoveBlock::MoveBlock(Models *model, glm::vec3 position, int id, glm::vec3 scale, int x, int y)
{
	this->id = id;
	this->model = model;
	this->position = position;
	modelMatrix = glm::translate(glm::mat4(1.0f), position) * glm::scale(glm::mat4(1.0f), scale);
	xAndy = glm::vec2(x, y);
}

MoveBlock::~MoveBlock()
{
}

void MoveBlock::draw(Camera * camera, ShaderHandler::ShaderProgram * shaderProgram, LightHandler * lightHandler)
{

	if (drawIt)
	{
		model->draw(camera, shaderProgram, lightHandler, &modelMatrix, color, id);
	}
}

void MoveBlock::update(Move move)
{
	drawIt = true;
	
	switch (move)
	{
	case Move::EmptySpace:
		color = Color::EMPTYSPACE;
		break;
	case Move::EnemySpace:
		color = Color::ENEMYSPACE;
		break;
	case Move::SpecialSpace:
		color = Color::SPECIALSPACE;
		break;
	case Move::NoneLegalSpace:
		drawIt = false;
		color = Color::NONELEAGALSPACE;
		break;
	case Move::OwnPiece:
		color = Color::OWNPIECE;
		break;
	}
	
}

void MoveBlock::reset()
{
	update(Move::NoneLegalSpace);
}
