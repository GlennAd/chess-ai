#pragma once

#if defined(__linux__)						// If we are using linux.
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#endif

#include <glm/gtc/type_ptr.hpp>

#include <vector>


#include "ShaderHandler.h"
#include "Framebuffer.h"

struct Light
{
	const float Constant = 1.0f;
	float linear;
	float quadratic;
	glm::vec3 position;
	glm::vec3 diffuse;
	glm::vec3 specular;
	glm::vec3 direction;
	float cutoff;
	float outerCutoff;
};

class LightHandler
{
public:
	LightHandler(glm::vec3 Playerpos);

	void update(glm::vec3 pos, glm::vec3 dir);
	void switchLight();
	Light getLight();
	bool getLightSwitch();

	void createProjectionMatrixes(Framebuffer& framebuffer, std::vector<glm::mat4> shadowTransforms, ShaderHandler::ShaderProgram& shaderProgram, const GLfloat& farPlane);
private:
	//--------------------------------PRIVATE VARIABLES----------------------------------
	Light light;
	bool lightSwitch;
};
