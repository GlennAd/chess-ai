#pragma once

#if defined(__linux__)
#include <glm/glm.hpp>
#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#include <glm\glm.hpp>
#endif


#include "ModelHandler.h"

enum Move
{
	EmptySpace,
	EnemySpace,
	SpecialSpace,
	NoneLegalSpace,
	OwnPiece
};

class MoveBlock
{
public:
	MoveBlock(Models *model, glm::vec3 position, int id, glm::vec3 scale, int x, int y);
	~MoveBlock();
	
	void draw(Camera * camera, ShaderHandler::ShaderProgram * shaderProgram, LightHandler * lightHandler);
	void update(Move move);
	void reset();
	glm::vec3 getPos() { return position; }
	int getId() { return id; }
	glm::vec2 getXandY() { return xAndy; }
	Color getColor() { return color; }
private:
	glm::mat4 modelMatrix;
	glm::vec3 position;
	glm::vec2 xAndy;
	Color color;
	bool drawIt;
	Models* model;
	int id;
};
