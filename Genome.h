#pragma once

#include "Gene.h"
#include "Link.h"
#include "Node.h"
#include <vector>

class Genome
{
public:
	Genome(int rows, int columns, int inputNodes, int outputNodes);
	Genome(int id);
	Genome(Genome* genome);
	~Genome();

	void mutate();
	void input(float input[], float output[]);
	void writeToFile(int id);


	//void draw(sf::RenderWindow& window);

private:
	void makeRandomOutLink(Node* node);
	void makeRandomInLink(Node* node);


	std::vector<Gene*> genes;
	std::vector<Link*> geneLinks;
	std::vector<Node*> inputNodes;
	std::vector<Node*> outputNodes;

	int genomeLength;
};

