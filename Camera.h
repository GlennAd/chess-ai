#pragma once

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.

#if defined(__linux__)
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL2/SDL.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL2\SDL.h>
#endif

class Camera
{
public:
	Camera() {};
	Camera(glm::vec3 position);

	void lookAt(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 front = glm::vec3(0.0f, 0.0f, 0.0f));

	void setPosition(glm::vec3 pos);
	void setProjection(float cameraNear, float cameraFar, float fov = glm::quarter_pi<float>() * 1.5f);
	void lookAround(glm::vec2 pos);

	glm::vec3 getPos();
	glm::vec3 getUp();
	glm::vec3 getFront();

	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();

	bool getFirstPerson();

	void setFront(glm::vec3 front);

	void toggleFirstPerson();
	//Used for camera movement while debugging

private:
	//--------------------------------PRIVATE VARIABLES----------------------------------
	bool firstPerson;

	glm::vec3 position;

	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

	glm::vec3 front = glm::vec3(1.0f, 0.0f, 0.0f);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec2 oldMousePosition = glm::vec2(0.0f, 0.0f);
	
	glm::vec2 last = glm::vec2(0);

	float yaw = 0.0f;
	float pitch = 0.0f;
};