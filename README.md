# Chess AI #

This repo is for the final AI assignment, where we are creating a chess AI that runs solely on NEAT (a neural network that evolves).

### Team Members ###

* Benjamin Gordon Wendling
* Glenn Adrian Nordhus
* Christoffer Berg Kongsness