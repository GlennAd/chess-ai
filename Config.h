#pragma once
#include <random>

extern float weightChangeMutation;
extern float newNodeMutation;
extern float newColumnMutation;
extern float newLinkMutation;


extern float deleteNodeMutation;
extern float deleteLinkMutation;

extern int ageModifierLimit;
extern int maxColumnsInAGene;
extern int maxNodesInGene;
extern int maxLinksOutOfNode;
extern float breedingThreshold;
extern float energyCostForBreeding;

extern int lengthBetweenNodes;
extern int lengthBetweenGenes;

extern std::mt19937 randomGen;
extern std::random_device randomSeeder;
extern std::uniform_real_distribution<float> percentageDistributor;
extern std::uniform_int_distribution<int> coinflipDistributor;