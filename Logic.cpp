#include "Logic.h"
#include <stdio.h>
#include <fstream>

Logic::Logic()
{
	enPassanteTile = "-";
	stockfishBoardString = "";
	int id = 0;
	turn = 0;
	turnCounter = 0;
	for (size_t i = 0; i < CHESSROW; i++)
	{
		std::vector<tile> tempBoard;
		for (size_t g = 0; g < CHESSROW; g++)
		{
			tile temp;
			temp.side = -1;
			temp.type = ChessPieces::NOPIECE;
			temp.x = i;
			temp.y = g;
			temp.pieceId = -1;
			temp.castlingRights = false;
			temp.move = Move::NoneLegalSpace;
			temp.pawnCounter = -1;
			tempBoard.push_back(temp);
		}
		chessBoard.push_back(tempBoard);
	}

	for (size_t i = 0; i < CHESSROW; i++)
	{
		chessBoard.at(1).at(i).side = 0;
		chessBoard.at(1).at(i).type = ChessPieces::PAWN;
		chessBoard.at(1).at(i).pieceId = id++;
	}
	for (size_t i = 0; i < CHESSROW; i++)
	{
		chessBoard.at(6).at(i).side = 1;
		chessBoard.at(6).at(i).type = ChessPieces::PAWN;
		chessBoard.at(6).at(i).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(0).at(i * 7).side = 0;
		chessBoard.at(0).at(i * 7).type = ChessPieces::ROOK;
		chessBoard.at(0).at(i * 7).pieceId = id++;
		chessBoard.at(0).at(i * 7).castlingRights = true;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(7).at(i * 7).side = 1;
		chessBoard.at(7).at(i * 7).type = ChessPieces::ROOK;
		chessBoard.at(7).at(i * 7).pieceId = id++;
		chessBoard.at(7).at(i * 7).castlingRights = true;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(0).at(2 + i * 3).side = 0;
		chessBoard.at(0).at(2 + i * 3).type = ChessPieces::BISHOP;
		chessBoard.at(0).at(2 + i * 3).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(7).at(2 + i * 3).side = 1;
		chessBoard.at(7).at(2 + i * 3).type = ChessPieces::BISHOP;
		chessBoard.at(7).at(2 + i * 3).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(0).at(1 + i * 5).side = 0;
		chessBoard.at(0).at(1 + i * 5).type = ChessPieces::KNIGHT;
		chessBoard.at(0).at(1 + i * 5).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(7).at(1 + i * 5).side = 1;
		chessBoard.at(7).at(1 + i * 5).type = ChessPieces::KNIGHT;
		chessBoard.at(7).at(1 + i * 5).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(i * 7).at(3).side = 0 + i;
		chessBoard.at(i * 7).at(3).type = ChessPieces::KING;
		chessBoard.at(i * 7).at(3).pieceId = id++;
		chessBoard.at(i * 7).at(3).castlingRights = true;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(i * 7).at(4).side = 0 + i;
		chessBoard.at(i * 7).at(4).type = ChessPieces::QUEEN;
		chessBoard.at(i * 7).at(4).pieceId = id++;
	}
	id++;
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t g = 0; g < CHESSROW; g++)
		{
			chessBoard.at(i).at(g).tileId = id;
			id++;
		}
	}


	std::ifstream file("./resources/genomes/config.txt");

	int currentGenomeId;
	file >> currentGenomeId;

	if(currentGenomeId != 0)
	{
		whiteBrain = new Genome(currentGenomeId);
		blackBrain = new Genome(whiteBrain);

		whiteBrain->mutate();
		blackBrain->mutate();
	}
	else
	{
		whiteBrain = new Genome(4, 4, 70, 1);
		blackBrain = new Genome(4, 4, 70, 1);
	
		for (int i = 0; i < 10; ++i)
		{
			whiteBrain->mutate();
			blackBrain->mutate();
		}
	}

	genomeIDCounter = currentGenomeId;
}

Logic::~Logic()
{
	delete whiteBrain;
	delete blackBrain;
}

bool Logic::checkForLegalMoves(int id)
{
	reset();
	glm::vec2 piece = findPiece(id);
	if (piece.x != -1)
	{
		if (checkTurn(chessBoard.at(piece.x).at(piece.y).side))
		{
			chessBoard.at(piece.x).at(piece.y).move = Move::OwnPiece;
			tile temp = chessBoard.at(piece.x).at(piece.y);

			switch (temp.type)
			{
			case ChessPieces::PAWN:
				checkPawn(piece, temp);
				break;
			case ChessPieces::ROOK:
				checkRook(piece, temp);
				break;
			case ChessPieces::BISHOP:
				checkBishop(piece, temp);
				break;
			case ChessPieces::KNIGHT:
				checkKnight(piece, temp);
				break;
			case ChessPieces::QUEEN:
				checkQueen(piece, temp);
				break;
			case ChessPieces::KING:
				checkKing(piece, temp);
				break;
			}
			return true;
		}
	}
	return false;
}

std::vector<std::vector<tile>>* Logic::getChessBoard()
{
	return &chessBoard;
}

std::vector<std::vector<Move>> Logic::LegalMoves()
{
	std::vector<std::vector<Move>> moves;
	for (size_t i = 0; i < chessBoard.size(); i++)
	{
		std::vector<Move> tempMove;
		for (size_t f = 0; f < chessBoard.at(i).size(); f++)
		{
			Move temp = chessBoard.at(f).at(i).move;
			tempMove.push_back(temp);
		}
		moves.push_back(tempMove);
	}

	return moves;
}

void Logic::doLegalMove(int pieceID, glm::vec2 tileNew, ChessPieces pawnUpgradeType)
{
	pieceMoved = pieceID;
	newLocation = tileNew;
	tile temp2, temp;
	ChessPieces oldPiece = chessBoard.at(tileNew.x).at(tileNew.y).type;
	if (tileNew.x != 500)
	{
		for (size_t i = 0; i < chessBoard.size(); i++)
		{
			for (size_t f = 0; f < chessBoard.at(i).size(); f++)
			{
				if (chessBoard.at(i).at(f).pieceId == pieceID)
				{
					temp2 = chessBoard.at(i).at(f);
					temp.side = -1;
					temp.type = ChessPieces::NOPIECE;
					temp.x = i;
					temp.y = f;
					temp.tileId = temp2.tileId;
					temp.pieceId = -1;
					temp.move = Move::NoneLegalSpace;
					temp.castlingRights = false;
					temp.pawnCounter = -1;
					chessBoard.at(i).at(f) = temp;
				}
			}
		}
		temp2.x = tileNew.x;
		temp2.y = tileNew.y;
		temp2.castlingRights = false;
		temp2.tileId = chessBoard.at(tileNew.x).at(tileNew.y).tileId;
		chessBoard.at(tileNew.x).at(tileNew.y) = temp2;
		if (temp2.type == PAWN)
		{
			if (turn == 0 && temp.x == 1 && temp2.x == 3)	
			{
				enPassanteTile = ' ';
				switch (temp.y)
				{
				case 7: enPassanteTile = 'a'; break;
				case 6: enPassanteTile = 'b'; break;
				case 5: enPassanteTile = 'c'; break;
				case 4: enPassanteTile = 'd'; break;
				case 3: enPassanteTile = 'e'; break;
				case 2: enPassanteTile = 'f'; break;
				case 1: enPassanteTile = 'g'; break;
				case 0: enPassanteTile = 'h'; break;
				}
				enPassanteTile.append(std::to_string(temp.x + 1));
			}
			else if (turn == 1 && temp.x == 6 && temp2.x == 4 )
			{
				enPassanteTile = ' ';
				switch (temp.y)
				{
				case 7: enPassanteTile = 'a'; break;
				case 6: enPassanteTile = 'b'; break;
				case 5: enPassanteTile = 'c'; break;
				case 4: enPassanteTile = 'd'; break;
				case 3: enPassanteTile = 'e'; break;
				case 2: enPassanteTile = 'f'; break;
				case 1: enPassanteTile = 'g'; break;
				case 0: enPassanteTile = 'h'; break;
				}
				enPassanteTile.append(std::to_string(temp.y + 1));
			}
		}
		else
		{
			enPassanteTile = "-";
		}
		if (temp2.type == PAWN && temp.x == 5 && turn == 0)
		{
			if (temp2.y == temp.y + 1 && chessBoard.at(tileNew.x).at(tileNew.y).side == -1)
			{
				chessBoard.at(tileNew.x-1).at(tileNew.y).pieceId = -1;
				chessBoard.at(tileNew.x-1).at(tileNew.y).type = NOPIECE;
			}
			else if (temp2.y == temp.y - 1 && chessBoard.at(tileNew.x).at(tileNew.y).side == -1)
			{
				chessBoard.at(tileNew.x-1).at(tileNew.y).pieceId = -1;
				chessBoard.at(tileNew.x-1).at(tileNew.y).type = NOPIECE;
			}
		}
		if (temp2.type == PAWN && temp.x == 4 && turn == 1)
		{
			if (temp2.y == temp.y + 1 && chessBoard.at(tileNew.x).at(tileNew.y).side == -1)
			{
				chessBoard.at(tileNew.x - 1).at(tileNew.y).pieceId = -1;
				chessBoard.at(tileNew.x - 1).at(tileNew.y).type = NOPIECE;
			}
			else if (temp2.y == temp.y - 1 && chessBoard.at(tileNew.x).at(tileNew.y).side == -1)
			{
				chessBoard.at(tileNew.x - 1).at(tileNew.y).pieceId = -1;
				chessBoard.at(tileNew.x - 1).at(tileNew.y).type = NOPIECE;
			}
		}
		addToStockfishBoardString(glm::vec2(temp.x, temp.y), tileNew);
		if (oldPiece == NOPIECE && temp2.type != PAWN)
		{
			fiftyMovesCounter++;
		}
		else
		{
			fiftyMovesCounter = 0;
		}
		if (chessBoard.at(tileNew.x).at(tileNew.y).type == KING)
		{
			if (temp.x == turn * 7 && temp.y == 3)
			{
				if (tileNew.y == 1)
				{
					temp.x = turn * 7;
					temp.y = 0;

					temp2 = chessBoard.at(turn * 7).at(0);
					temp.tileId = temp2.tileId;

					chessBoard.at(turn * 7).at(0) = temp;

					temp2.x = turn * 7;
					temp2.y = 2;

					temp2.tileId = chessBoard.at(turn * 7).at(2).tileId;
					chessBoard.at(turn * 7).at(2) = temp2;
				}
				if (tileNew.y == 5)
				{
					temp.x = turn * 7;
					temp.y = 7;

					temp2 = chessBoard.at(turn * 7).at(7);
					temp.tileId = temp2.tileId;

					chessBoard.at(turn * 7).at(7) = temp;

					temp2.x = turn * 7;
					temp2.y = 4;
					temp2.tileId = chessBoard.at(turn * 7).at(4).tileId;
					chessBoard.at(turn * 7).at(4) = temp2;
				}
			}
		}
		if (turn == 0 && chessBoard.at(tileNew.x).at(tileNew.y).type == PAWN && tileNew.x == 7)
		{
 			if (pawnUpgradeType != NOPIECE)
			{
				chessBoard.at(tileNew.x).at(tileNew.y).type = pawnUpgradeType;

				if (pawnUpgradeType == QUEEN) { stockfishBoardString.append("q"); }
				if (pawnUpgradeType == KNIGHT) { stockfishBoardString.append("k"); }
				if (pawnUpgradeType == BISHOP) { stockfishBoardString.append("b"); }
				if (pawnUpgradeType == ROOK) { stockfishBoardString.append("r"); }
			}
			else
			{
				this->pawnUpgrade = true;
				pawnToBeUpgraded = tileNew;
			}
			//	std::cout << "im in here\n\n";
		}
		else if (turn == 1 && chessBoard.at(tileNew.x).at(tileNew.y).type == PAWN && tileNew.x == 0)
		{
			if (pawnUpgradeType != NOPIECE)
			{
				chessBoard.at(tileNew.x).at(tileNew.y).type = pawnUpgradeType;

				if (pawnUpgradeType == QUEEN) { stockfishBoardString.append("q"); }
				if (pawnUpgradeType == KNIGHT) { stockfishBoardString.append("k"); }
				if (pawnUpgradeType == BISHOP) { stockfishBoardString.append("b"); }
				if (pawnUpgradeType == ROOK) { stockfishBoardString.append("r"); }
			}
			else
			{
				this->pawnUpgrade = true;
				pawnToBeUpgraded = tileNew;
			}
		}
		if (turn == 0 && chessBoard.at(tileNew.x).at(tileNew.y).type == PAWN && tileNew.x == 3 && temp.x == 1)
		{
			chessBoard.at(tileNew.x).at(tileNew.y).pawnCounter = turnCounter;
		}
		if (turn == 1 && chessBoard.at(tileNew.x).at(tileNew.y).type == PAWN && tileNew.x == 4 && temp.x == 6)
		{
			chessBoard.at(tileNew.x).at(tileNew.y).pawnCounter = turnCounter;
		}
		if (turn == 0)
		{
			turn = 1;
		}
		else
		{
			turn = 0;
		}
		turnCounter++;
		if (checkMate(turn))
		{
			++gameCount;
			resetLogic();
		}
		//std::cout << stockfishBoardString << std::endl;
		//printf("%s\n", stockfishBoardString.c_str());
	}
}

glm::vec2 Logic::upgradePawn(int actor)
{
	switch (actor)
	{
	case 100: chessBoard[pawnToBeUpgraded.x][pawnToBeUpgraded.y].type = QUEEN; stockfishBoardString.append("q"); break;
	case 101: chessBoard[pawnToBeUpgraded.x][pawnToBeUpgraded.y].type = KNIGHT; stockfishBoardString.append("k");  break;
	case 102: chessBoard[pawnToBeUpgraded.x][pawnToBeUpgraded.y].type = BISHOP; stockfishBoardString.append("b"); break;
	case 103: chessBoard[pawnToBeUpgraded.x][pawnToBeUpgraded.y].type = ROOK; stockfishBoardString.append("r"); break;
	}
	pawnUpgrade = false;
	checkMate(turn);
	return pawnToBeUpgraded;
}

void Logic::reset()
{
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t g = 0; g < CHESSROW; g++)
		{
			chessBoard.at(i).at(g).move = Move::NoneLegalSpace;
		}
	}
}

bool Logic::checkTurn(int side)
{
	return (turn == side);
}

bool Logic::check(int side) // if i do this am i still in check/ then in check
{
	tile temp;
	int otherSide;
	bool check = false;
	bool blocked;
	int x, y;
	if (turn == 0)
	{
		otherSide = 1;
	}
	else
	{
		otherSide = 0;
	}
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t f = 0; f < CHESSROW; f++)
		{
			if (boardAfterMove.at(i).at(f).side == side && boardAfterMove.at(i).at(f).type == ChessPieces::KING)
			{
				temp = boardAfterMove.at(i).at(f);
			}
		}
	}
	if (turn == 0)
	{
		for (int i = -1; i < 2; i += 2)
		{
			if (temp.x + 1 <= 7 && temp.y + i <= 7 && temp.x + 1 >= 0 && temp.y + i >= 0)
			{
				if (boardAfterMove.at(temp.x + 1).at(temp.y + i).side == otherSide && boardAfterMove.at(temp.x + 1).at(temp.y + i).type == PAWN)
				{
					check = true;
				}
			}
		}
	}
	else
	{
		for (int i = -1; i < 2; i += 2)
		{
			if (temp.x - 1 <= 7 && temp.y + i <= 7 && temp.x - 1 >= 0 && temp.y + i >= 0)
			{
				if (boardAfterMove.at(temp.x - 1).at(temp.y + i).side == otherSide && boardAfterMove.at(temp.x- 1).at(temp.y + i).type == PAWN)
				{
					check = true;
				}
			}
		}
	}
	for (size_t y = 1, x = 2; y <= 2; y++, x--) // frist x = 2 y = 1 last x = 1 y = 2
	{
		for (int g = -1; g <= 1; g += 2)
		{
			for (int d = -1; d <= 1; d += 2)
			{
				int newX = x*g;
				int newY = y*d;
				if (temp.x + newX >= 0 && temp.y + newY >= 0 && temp.x + newX <= 7 && temp.y + newY <= 7)
				{
					if (boardAfterMove.at(temp.x + newX).at(temp.y + newY).side != turn)
					{
						if (boardAfterMove.at(temp.x + newX).at(temp.y + newY).side == otherSide)
						{
							if (boardAfterMove.at(temp.x + newX).at(temp.y + newY).type == KNIGHT)
							{
								check = true;
							}
						}
					}
				}
			}
		}
	}
	for (int i = -1; i < 2; i++)
	{
		for (int f = -1; f < 2; f++)
		{
			x = 0;
			y = 0;
			blocked = false;
			while ((temp.x + x >= 0 && temp.y + y >= 0 && temp.x + x <= 7 && temp.y + y <= 7) && !blocked)
			{
				x += 1 * i;
				y += 1 * f;

				if (temp.x + x >= 0 && temp.y + y >= 0 && temp.x + x <= 7 && temp.y + y <= 7)
				{
					if (boardAfterMove.at(temp.x + x).at(temp.y + y).side != turn)
					{
						if (boardAfterMove.at(temp.x + x).at(temp.y + y).side == otherSide)
						{
							if (boardAfterMove.at(temp.x + x).at(temp.y + y).type == QUEEN || (abs(x) - abs(y) == 0 && boardAfterMove.at(temp.x + x).at(temp.y + y).type == BISHOP) ||
								(abs(x) - abs(y) != 0 && boardAfterMove.at(temp.x + x).at(temp.y + y).type == ROOK))
							{
								check = true;
								blocked = true;
							}
						}
					}
					else
					{
						blocked = true;
					}
				}
				if (x == 0 && y == 0) // get out of an forever loop
				{
					blocked = true;
				}
			}
			if (temp.x + i >= 0 && temp.y + f >= 0 && temp.x + i <= 7 && temp.y + f <= 7)
			{
				if (boardAfterMove.at(temp.x + i).at(temp.y + f).side == otherSide)
				{
					if (boardAfterMove.at(temp.x + i).at(temp.y + f).type == KING)
					{
						check = true;
					}
					blocked = true;
				}
			}
			else
			{
				blocked = true;
			}

			if (i == 0 && f == 0) // get out of an forever loop
			{
				blocked = true;
			}
		}
	}
	return check;
}

bool Logic::checkMate(int side)
{
	glm::vec2 piece;
	bool finished = true;
	int otherside;
	reset();
	if (side == 0)
	{
		otherside = 1;
	}
	else
	{
		otherside = 0;
	}
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t f = 0; f < CHESSROW; f++)
		{
			if (chessBoard.at(i).at(f).side == side)
			{
				piece = glm::vec2(i, f);
				tile temp = chessBoard.at(i).at(f);
				switch (temp.type)
				{
				case ChessPieces::PAWN:
					checkPawn(piece, temp);
					break;
				case ChessPieces::ROOK:
					checkRook(piece, temp);
					break;
				case ChessPieces::BISHOP:
					checkBishop(piece, temp);
					break;
				case ChessPieces::KNIGHT:
					checkKnight(piece, temp);
					break;
				case ChessPieces::QUEEN:
					checkQueen(piece, temp);
					break;
				case ChessPieces::KING:
					checkKing(piece, temp);
					break;
				}
			}
		}
	}
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t f = 0; f < CHESSROW; f++)
		{
			if (chessBoard.at(i).at(f).move == EnemySpace || chessBoard.at(i).at(f).move == EmptySpace)
			{
				//std::cout << i << "x " << f << "y" << chessBoard.at(i).at(f).move << "and" << chessBoard.at(i).at(f).type << std::endl;
				finished = false;
			}
		}
	}

	tile kingTile = findKingTile(side);
	if (check(side) && finished)
	{
		if (turn == 0)
		{
			std::cout << "Black wins!\n";
		}
		else
		{
			std::cout << "White wins!\n";
		}
	}
	else if (finished)
	{
		std::cout << "Stalemate!\n";
	}
	if (turnCounter > MAXTURN)
	{
		std::cout << "Stalemate!\n";
		finished = true;
	}
	if (treeSteps())
	{
		std::cout << "Stalemate!\n";
		finished = true;
	}
	if (fiftyMovesCounter > 50)
	{
		std::cout << "Stalemate!\n";
		finished = true;
	}

	if (finished)
	{
		printf("This shuold be called once per game end\n");
	}


	return finished;
}

std::vector<moveInfo> Logic::getAllMoves()
{
	moves.clear();
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t f = 0; f < CHESSROW; f++)
		{
			if (chessBoard.at(i).at(f).side == turn)
			{
				checkForLegalMoves(chessBoard.at(i).at(f).pieceId);
			}
		}
	}
	reset();
	return moves;
}

void Logic::resetLogic()
{
	stockfishBoardString = "";
	int id = 0;
	turn = 0;
	turnCounter = 0;
	fiftyMovesCounter = 0;
	chessBoard.clear();
	for (size_t i = 0; i < CHESSROW; i++)
	{
		std::vector<tile> tempBoard;
		for (size_t g = 0; g < CHESSROW; g++)
		{
			tile temp;
			temp.side = -1;
			temp.type = ChessPieces::NOPIECE;
			temp.x = i;
			temp.y = g;
			temp.pieceId = -1;
			temp.castlingRights = false;
			temp.move = Move::NoneLegalSpace;
			temp.pawnCounter = -1;
			tempBoard.push_back(temp);
		}
		chessBoard.push_back(tempBoard);
	}

	for (size_t i = 0; i < CHESSROW; i++)
	{
		chessBoard.at(1).at(i).side = 0;
		chessBoard.at(1).at(i).type = ChessPieces::PAWN;
		chessBoard.at(1).at(i).pieceId = id++;
	}
	for (size_t i = 0; i < CHESSROW; i++)
	{
		chessBoard.at(6).at(i).side = 1;
		chessBoard.at(6).at(i).type = ChessPieces::PAWN;
		chessBoard.at(6).at(i).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(0).at(i * 7).side = 0;
		chessBoard.at(0).at(i * 7).type = ChessPieces::ROOK;
		chessBoard.at(0).at(i * 7).pieceId = id++;
		chessBoard.at(0).at(i * 7).castlingRights = true;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(7).at(i * 7).side = 1;
		chessBoard.at(7).at(i * 7).type = ChessPieces::ROOK;
		chessBoard.at(7).at(i * 7).pieceId = id++;
		chessBoard.at(7).at(i * 7).castlingRights = true;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(0).at(2 + i * 3).side = 0;
		chessBoard.at(0).at(2 + i * 3).type = ChessPieces::BISHOP;
		chessBoard.at(0).at(2 + i * 3).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(7).at(2 + i * 3).side = 1;
		chessBoard.at(7).at(2 + i * 3).type = ChessPieces::BISHOP;
		chessBoard.at(7).at(2 + i * 3).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(0).at(1 + i * 5).side = 0;
		chessBoard.at(0).at(1 + i * 5).type = ChessPieces::KNIGHT;
		chessBoard.at(0).at(1 + i * 5).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(7).at(1 + i * 5).side = 1;
		chessBoard.at(7).at(1 + i * 5).type = ChessPieces::KNIGHT;
		chessBoard.at(7).at(1 + i * 5).pieceId = id++;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(i * 7).at(3).side = 0 + i;
		chessBoard.at(i * 7).at(3).type = ChessPieces::KING;
		chessBoard.at(i * 7).at(3).pieceId = id++;
		chessBoard.at(i * 7).at(3).castlingRights = true;
	}
	for (size_t i = 0; i < 2; i++)
	{
		chessBoard.at(i * 7).at(4).side = 0 + i;
		chessBoard.at(i * 7).at(4).type = ChessPieces::QUEEN;
		chessBoard.at(i * 7).at(4).pieceId = id++;
	}
	id++;
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t g = 0; g < CHESSROW; g++)
		{
			chessBoard.at(i).at(g).tileId = id;
			id++;
		}
	}
}

void Logic::aiMove()
{
	std::vector<moveInfo> legalMoves = getAllMoves();

	if (turn == 0)
	{
		float inputs[70];

		int j = 0;

		for (int x = 0; x < chessBoard.size(); ++x)
		{
			for (int y = 0; y < chessBoard[x].size(); ++y, ++j)
			{
				if (chessBoard[x][y].side == turn)
				{
					inputs[j] = chessBoard[x][y].type / 6.0f;
				}
				else
				{
					inputs[j] = -(chessBoard[x][y].type / 6.0f);
				}
			}
		}

		for (int i = 0; i < legalMoves.size(); ++i)
		{
			inputs[j] = legalMoves[i].startX / 8.0f;
			inputs[j + 1] = legalMoves[i].startY / 8.0f;
			inputs[j + 2] = legalMoves[i].endX / 8.0f;
			inputs[j + 3] = legalMoves[i].endY / 8.0f;
			inputs[j + 4] = legalMoves[i].typeMoved / 6.0f;
			inputs[j + 5] = -(legalMoves[i].atLocation / 6.0f);

			float output[1];

			whiteBrain->input(inputs, output);

			legalMoves[i].score = output[0];
		}

		//printf("Score for first from white: %f\n", legalMoves.at(0).score);
	}
	else
	{
		float inputs[70];

		int j = 0;

		for (int x = 0; x < chessBoard.size(); ++x)
		{
			for (int y = 0; y < chessBoard[x].size(); ++y, ++j)
			{
				if (chessBoard[x][y].side == turn)
				{
					inputs[j] = chessBoard[x][y].type / 6.0f;
				}
				else
				{
					inputs[j] = -(chessBoard[x][y].type / 6.0f);
				}
			}
		}

		for (int i = 0; i < legalMoves.size(); ++i)
		{
			inputs[j] = legalMoves[i].startX / 8.0f;
			inputs[j + 1] = legalMoves[i].startY / 8.0f;
			inputs[j + 2] = legalMoves[i].endX / 8.0f;
			inputs[j + 3] = legalMoves[i].endY / 8.0f;
			inputs[j + 4] = legalMoves[i].typeMoved / 6.0f;
			inputs[j + 5] = -(legalMoves[i].atLocation / 6.0f);

			float output[1];

			blackBrain->input(inputs, output);

			legalMoves[i].score = output[0];
		}

		//printf("Score for first from black: %f\n", legalMoves.at(0).score);
	}

	moveInfo bestMove = legalMoves.at(0);
	for (int i = 1; i < legalMoves.size(); ++i)
	{
		if (legalMoves[i].score > bestMove.score)
		{
			bestMove = legalMoves[i];
		}
	}

	int pieceID = chessBoard[bestMove.startX][bestMove.startY].pieceId;

	doLegalMove(pieceID, glm::vec2(bestMove.endX, bestMove.endY), bestMove.typeMoved);
}

void Logic::getScreenInfo(int &piece, int &newLoc)
{
	piece = this->pieceMoved;
	newLoc = 33 + (newLocation.x) + newLocation.y * 8;
}

void Logic::checkPawn(glm::vec2 piece, tile pieceLoacation)
{
	if (pieceLoacation.side == 0) // what side is the piece on
	{
		if (pieceLoacation.x < 7) // is it at the end
		{
			if (chessBoard.at(piece.x + 1).at(piece.y).pieceId == -1 && !illegalMove(piece, glm::vec2(piece.x + 1, piece.y)))
			{
				chessBoard.at(piece.x + 1).at(piece.y).move = Move::EmptySpace;

				if (pieceLoacation.x == 1 && chessBoard.at(piece.x + 2).at(piece.y).pieceId == -1)
				{
					if (!illegalMove(piece, glm::vec2(piece.x + 2, piece.y)))
					{
						chessBoard.at(3).at(piece.y).move = Move::EmptySpace;
						addMove(piece, glm::vec2(piece.x + 2, piece.y));
					}
				}
				if (pieceLoacation.x == 6)
				{
					addMove(piece, glm::vec2(piece.x + 1, piece.y), ChessPieces::BISHOP);
					addMove(piece, glm::vec2(piece.x + 1, piece.y), ChessPieces::KNIGHT);
					addMove(piece, glm::vec2(piece.x + 1, piece.y), ChessPieces::ROOK);
					addMove(piece, glm::vec2(piece.x + 1, piece.y), ChessPieces::QUEEN);
				}
				else
				{
					addMove(piece, glm::vec2(piece.x + 1, piece.y));
				}
			}
		}
		for (int i = -1; i < 2; i += 2)
		{
			if (piece.y + i >= 0 && piece.y + i < 8 && piece.x + 1 < 8 )
			{
				if (chessBoard.at(piece.x + 1).at(piece.y + i).side == 1 && !illegalMove(piece, glm::vec2(piece.x + 1, piece.y + i)))
				{
					chessBoard.at(piece.x + 1).at(piece.y + i).move = Move::EnemySpace;
					if (pieceLoacation.x == 6)
					{
						addMove(piece, glm::vec2(piece.x + 1, piece.y + i), ChessPieces::BISHOP);
						addMove(piece, glm::vec2(piece.x + 1, piece.y + i), ChessPieces::KNIGHT);
						addMove(piece, glm::vec2(piece.x + 1, piece.y + i), ChessPieces::ROOK);
						addMove(piece, glm::vec2(piece.x + 1, piece.y + i), ChessPieces::QUEEN);
					}
					else
					{
						addMove(piece, glm::vec2(piece.x + 1, piece.y + i));
					}
				}
			}
		}
		if (pieceLoacation.x == 4)
		{
			for (int i = -1; i < 2; i += 2)
			{
				if (piece.y + i >= 0 && piece.y + i < 8)
				{
					if (chessBoard.at(piece.x).at(piece.y + i).side == 1 && chessBoard.at(piece.x).at(piece.y + i).type == PAWN && chessBoard.at(piece.x + 1).at(piece.y + i).pieceId == -1 && chessBoard.at(piece.x).at(piece.y + i).pawnCounter == (turnCounter - 1))
					{
						chessBoard.at(piece.x + 1).at(piece.y + i).move = Move::EnemySpace;
						addMove(piece, glm::vec2(piece.x + 1, piece.y + i));
					}
				}
			}
		}
	}
	else if (pieceLoacation.side == 1)
	{
		if (pieceLoacation.x > 0)
		{
			if (chessBoard.at(piece.x - 1).at(piece.y).pieceId == -1 && !illegalMove(piece, glm::vec2(piece.x - 1, piece.y)))
			{
				chessBoard.at(piece.x - 1).at(piece.y).move = Move::EmptySpace;

				if (pieceLoacation.x == 6 && chessBoard.at(piece.x - 2).at(piece.y).pieceId == -1)
				{
					if (!illegalMove(piece, glm::vec2(piece.x - 2, piece.y)))
					{
						chessBoard.at(4).at(piece.y).move = Move::EmptySpace;
						addMove(piece, glm::vec2(piece.x - 2, piece.y));
					}
				}
				if (pieceLoacation.x == 1)
				{
					addMove(piece, glm::vec2(piece.x - 1, piece.y), ChessPieces::BISHOP);
					addMove(piece, glm::vec2(piece.x - 1, piece.y), ChessPieces::KNIGHT);
					addMove(piece, glm::vec2(piece.x - 1, piece.y), ChessPieces::ROOK);
					addMove(piece, glm::vec2(piece.x - 1, piece.y), ChessPieces::QUEEN);
				}
				else
				{
					addMove(piece, glm::vec2(piece.x - 1, piece.y));
				}
			}
		}
		for (int i = -1; i < 2; i += 2)
		{
			if (piece.y + i >= 0 && piece.y + 1 <= 7 &&
				piece.x - 1 >= 0)
			{
				if (chessBoard.at(piece.x - 1).at(piece.y + i).side == 0 && !illegalMove(piece, glm::vec2(piece.x - 1, piece.y + i)))
				{
					chessBoard.at(piece.x - 1).at(piece.y + i).move = Move::EnemySpace;
					if (pieceLoacation.x == 1)
					{
						addMove(piece, glm::vec2(piece.x - 1, piece.y + i), ChessPieces::BISHOP);
						addMove(piece, glm::vec2(piece.x - 1, piece.y + i), ChessPieces::KNIGHT);
						addMove(piece, glm::vec2(piece.x - 1, piece.y + i), ChessPieces::ROOK);
						addMove(piece, glm::vec2(piece.x - 1, piece.y + i), ChessPieces::QUEEN);
					}
					else
					{
						addMove(piece, glm::vec2(piece.x - 1, piece.y + i));
					}
				}
			}
		}
		if (pieceLoacation.x == 3)
		{
			for (int i = -1; i < 2; i += 2)
			{
				if (piece.y + i >= 0 && piece.y + i < 8)
				{
					if (chessBoard.at(piece.x).at(piece.y + i).side == 0 && chessBoard.at(piece.x).at(piece.y + i).type == PAWN && chessBoard.at(piece.x - 1).at(piece.y + i).pieceId == -1 && chessBoard.at(piece.x).at(piece.y + i).pawnCounter == (turnCounter - 1))
					{
						chessBoard.at(piece.x - 1).at(piece.y + i).move = Move::EnemySpace;
						addMove(piece, glm::vec2(piece.x - 1, piece.y + i));
					}
				}
			}
		}
	}
}

void Logic::checkKnight(glm::vec2 piece, tile pieceLoacation)
{
	for (size_t y = 1, x = 2; y <= 2; y++, x--)
	{
		for (int g = -1; g <= 1; g += 2)
		{
			for (int d = -1; d <= 1; d += 2)
			{
				int newX = x*g;
				int newY = y*d;
				if (piece.x + newX >= 0 && piece.y + newY >= 0 && piece.x + newX <= 7 && piece.y + newY <= 7)
				{
					if (chessBoard.at(piece.x + newX).at(piece.y + newY).side != pieceLoacation.side && !illegalMove(piece, glm::vec2(piece.x + newX, piece.y + newY)))
					{
						if (chessBoard.at(piece.x + newX).at(piece.y + newY).side != -1)
						{
							chessBoard.at(piece.x + newX).at(piece.y + newY).move = Move::EnemySpace;
						}
						else
						{
							chessBoard.at(piece.x + newX).at(piece.y + newY).move = Move::EmptySpace;
						}
						addMove(piece, glm::vec2(piece.x + newX, piece.y + newY));
					}
				}
			}
		}
	}
}

void Logic::checkBishop(glm::vec2 piece, tile pieceLoacation)
{
	int x, y;

	bool blocked = false;
	for (int i = -1; i < 2; i += 2)
	{
		for (int f = -1; f < 2; f += 2)
		{
			blocked = false;
			x = 0, y = 0;
			while ((piece.x + x >= 0 && piece.y + y >= 0 && piece.x + x <= 7 && piece.y + y <= 7) && !blocked)
			{
				x += 1 * i;
				y += 1 * f;

				if (piece.x + x >= 0 && piece.y + y >= 0 && piece.x + x <= 7 && piece.y + y <= 7)
				{
					if (chessBoard.at(piece.x + x).at(piece.y + y).side != pieceLoacation.side && !illegalMove(piece, glm::vec2(piece.x + x, piece.y + y)))
					{
						if (chessBoard.at(piece.x + x).at(piece.y + y).side != -1)
						{
							chessBoard.at(piece.x + x).at(piece.y + y).move = Move::EnemySpace;
							blocked = true;
						}
						else
						{
							chessBoard.at(piece.x + x).at(piece.y + y).move = Move::EmptySpace;
						}
						addMove(piece, glm::vec2(piece.x + x, piece.y + y));
					}
					else
					{
						blocked = true;
					}
				}
			}
		}
	}
}

void Logic::checkRook(glm::vec2 piece, tile pieceLoacation)
{
	int x, y;

	bool blocked = false;
	for (int i = 0; i < 2; i++)
	{
		for (int f = -1; f < 2; f += 2)
		{
			blocked = false;
			x = 0, y = 0;
			while ((piece.x + x >= 0 && piece.y + y >= 0 && piece.x + x <= 7 && piece.y + y <= 7) && !blocked)
			{
				if (i == 0)
				{
					x += 1 * f;
				}
				else
				{
					y += 1 * f;
				}
				if (piece.x + x >= 0 && piece.y + y >= 0 && piece.x + x <= 7 && piece.y + y <= 7)
				{
					if (chessBoard.at(piece.x + x).at(piece.y + y).side != pieceLoacation.side && !illegalMove(piece, glm::vec2(piece.x + x, piece.y + y)))
					{
						if (chessBoard.at(piece.x + x).at(piece.y + y).side != -1)
						{
							chessBoard.at(piece.x + x).at(piece.y + y).move = Move::EnemySpace;
							blocked = true;
						}
						else
						{
							chessBoard.at(piece.x + x).at(piece.y + y).move = Move::EmptySpace;
						}
						addMove(piece, glm::vec2(piece.x + x, piece.y + y));
					}
					else
					{
						blocked = true;
					}
				}
			}
		}
	}
}

void Logic::checkQueen(glm::vec2 piece, tile pieceLoacation)
{
	checkBishop(piece, pieceLoacation);
	checkRook(piece, pieceLoacation);
}

void Logic::checkKing(glm::vec2 piece, tile pieceLoacation)
{
	for (int x = -1; x <= 1; x++)
	{
		for (int y = -1; y <= 1; y++)
		{
			if (x != 0 || y != 0)
			{
				if (piece.x + x >= 0 && piece.y + y >= 0 && piece.x + x <= 7 && piece.y + y <= 7)
				{
					if (chessBoard.at(piece.x + x).at(piece.y + y).side != pieceLoacation.side && !illegalMove(piece, glm::vec2(piece.x + x, piece.y + y)))
					{
						if (chessBoard.at(piece.x + x).at(piece.y + y).side != -1)
						{
							chessBoard.at(piece.x + x).at(piece.y + y).move = Move::EnemySpace;
						}
						else
						{
							chessBoard.at(piece.x + x).at(piece.y + y).move = Move::EmptySpace;
						}
						addMove(piece, glm::vec2(piece.x + x, piece.y + y));
					}
				}
			}
		}
	}
	boardAfterMove = chessBoard;
	if (chessBoard.at(piece.x).at(piece.y).castlingRights && !check(turn))
	{
		if (chessBoard.at(turn * 7).at(0).castlingRights)
		{
			if (chessBoard.at(turn * 7).at(1).side == -1 && chessBoard.at(turn * 7).at(2).side == -1)
			{
				if (!illegalMove(piece, glm::vec2(turn * 7, 2)))
				{
					chessBoard.at(turn * 7).at(1).move = Move::EmptySpace;
					addMove(piece, glm::vec2(turn * 7, 1));
				}
			}
		}
		if (chessBoard.at(turn * 7).at(7).castlingRights)
		{
			if (chessBoard.at(turn * 7).at(6).side == -1 && chessBoard.at(turn * 7).at(5).side == -1 && chessBoard.at(turn * 7).at(4).side == -1)
			{
				if (!illegalMove(piece, glm::vec2(turn * 7, 5)))
				{
					chessBoard.at(turn * 7).at(5).move = Move::EmptySpace;
					addMove(piece, glm::vec2(turn * 7, 5));
				}
			}
		}
	}
}

tile Logic::findKingTile(int side)
{
	tile temp;
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t f = 0; f < CHESSROW; f++)
		{
			if (chessBoard.at(i).at(f).side == side && chessBoard.at(i).at(f).type == ChessPieces::KING)
			{
				temp = chessBoard.at(i).at(f);
			}
		}
	}
	return temp;
}

bool Logic::illegalMove(glm::vec2 piece, glm::vec2 newPosition)
{
	bool illegal = false;
	boardAfterMove = chessBoard;
	tile temp;
	tile temp2 = boardAfterMove.at(newPosition.x).at(newPosition.y);
	tile temp3 = boardAfterMove.at(piece.x).at(piece.y);
	temp.side = -1;
	temp.type = ChessPieces::NOPIECE;
	temp.x = piece.x;
	temp.y = piece.y;
	temp.tileId = temp2.tileId;
	temp.pieceId = -1;
	temp.move = Move::NoneLegalSpace;
	boardAfterMove.at(piece.x).at(piece.y) = temp;
	temp = temp3;
	temp.x = newPosition.x;
	temp.y = newPosition.y;
	temp.tileId = temp2.tileId;
	boardAfterMove.at(newPosition.x).at(newPosition.y) = temp;

	// do stuff
	if (check(turn))
	{
		illegal = true;
	}
	// reset back
	boardAfterMove = chessBoard;
	return illegal;
}

void Logic::addToStockfishBoardString(glm::vec2 start, glm::vec2 finish)
{
	std::string temp = "";
	int startX = (int)start.x + 1;
	int startY = (int)start.y;
	int finishX = (int)finish.x + 1;
	int finishY = (int)finish.y;
	temp += ' ';
	switch (startY)
	{
	case 7: temp += 'a'; break;
	case 6: temp += 'b'; break;
	case 5: temp += 'c'; break;
	case 4: temp += 'd'; break;
	case 3: temp += 'e'; break;
	case 2: temp += 'f'; break;
	case 1: temp += 'g'; break;
	case 0: temp += 'h'; break;
	}
	temp.append(std::to_string(startX));
	switch (finishY)
	{
	case 7: temp += 'a'; break;
	case 6: temp += 'b'; break;
	case 5: temp += 'c'; break;
	case 4: temp += 'd'; break;
	case 3: temp += 'e'; break;
	case 2: temp += 'f'; break;
	case 1: temp += 'g'; break;
	case 0: temp += 'h'; break;
	}
	temp.append(std::to_string(finishX));
	stockfishBoardString.append(temp);
}

void Logic::addMove(glm::vec2 start, glm::vec2 finish, ChessPieces pawnUpgrade)
{
	moveInfo temp;
	temp.startX = start.x;
	temp.startY = start.y;
	temp.endX = finish.x;
	temp.endY = finish.y;
	if (pawnUpgrade != ChessPieces::NOPIECE)
	{
		temp.typeMoved = pawnUpgrade;
	}
	else
	{
		temp.typeMoved = chessBoard.at(start.x).at(start.y).type;
	}
	temp.atLocation = chessBoard.at(finish.x).at(finish.y).type;
	moves.push_back(temp);
}

bool Logic::treeSteps()
{
	bool treeSteps = false;
	std::string temp, temp2[12];
	if (turnCounter > 12)
	{
		int length = stockfishBoardString.length();
		std::string temp = stockfishBoardString.substr(stockfishBoardString.length() - 60);
		if (temp.at(0) == ' ')
		{
			for (size_t i = 0; i < 12; i++)
			{
				temp2[i] = temp.substr(i * 5, 5);
			}
			if (temp2[0] == temp2[4] && temp2[0] == temp2[8] && temp2[1] == temp2[5] && temp2[1] == temp2[9] && temp2[2] == temp2[6] && temp2[2] == temp2[10] && temp2[3] == temp2[7] && temp2[3] == temp2[11])
			{
				treeSteps = true;
			}
		}
	}
	return treeSteps;
}

glm::vec2 Logic::findPiece(int id)
{
	glm::vec2 temp(-1);
	for (size_t i = 0; i < chessBoard.size(); i++)
	{
		for (size_t f = 0; f < chessBoard.at(i).size(); f++)
		{
			if (chessBoard.at(i).at(f).pieceId == id)
			{
				temp = glm::vec2(chessBoard.at(i).at(f).x, chessBoard.at(i).at(f).y);
			}
		}
	}
	return temp;
}

glm::vec2 Logic::findTile(int id)
{
	glm::vec2 temp(-1);
	for (size_t i = 0; i < chessBoard.size(); i++)
	{
		for (size_t f = 0; f < chessBoard.at(i).size(); f++)
		{
			if (chessBoard.at(i).at(f).tileId == id)
			{
				temp = glm::vec2(i / 1.0f, f / 1.0f);
			}
		}
	}
	return temp;
}

std::string Logic::getFEN()
{
	std::string temp = "";
	int emptySpaces, moveClock;
	bool wcastle, bcastle;
	wcastle = true;
	bcastle = true;

	for(int i = 7; i>=0; --i)
	{
		emptySpaces = 0;
		for(int j = 7; j>=0; --j)
		{
			switch(chessBoard[i][j].type)
			{
				case ChessPieces::NOPIECE : ++emptySpaces;
											break;
				case ChessPieces::PAWN :	if(emptySpaces > 0)
											{
												temp.append(std::to_string(emptySpaces));
												emptySpaces = 0;
											}
											if(chessBoard[i][j].side == 0)
											{
												temp.append("P");
											}
											else if(chessBoard[i][j].side == 1)
											{
												temp.append("p");
											}
											break;
				case ChessPieces::KNIGHT :	if(emptySpaces > 0)
											{
												temp.append(std::to_string(emptySpaces));
												emptySpaces = 0;
											}
											if(chessBoard[i][j].side == 0)
											{
												temp.append("N");
											}
											else if(chessBoard[i][j].side == 1)
											{
												temp.append("n");
											}
											break;
				case ChessPieces::BISHOP :	if(emptySpaces > 0)
											{
												temp.append(std::to_string(emptySpaces));
												emptySpaces = 0;
											}
											if(chessBoard[i][j].side == 0)
											{
												temp.append("B");
											}
											else if(chessBoard[i][j].side == 1)
											{
												temp.append("b");
											}
											break;
				case ChessPieces::ROOK :	if(emptySpaces > 0)
											{
												temp.append(std::to_string(emptySpaces));
												emptySpaces = 0;
											}
											if(chessBoard[i][j].side == 0)
											{
												temp.append("R");
											}
											else if(chessBoard[i][j].side == 1)
											{
												temp.append("r");
											}
											break;
				case ChessPieces::QUEEN : 	if(emptySpaces > 0)
											{
												temp.append(std::to_string(emptySpaces));
												emptySpaces = 0;
											}
											if(chessBoard[i][j].side == 0)
											{
												temp.append("Q");
											}
											else if(chessBoard[i][j].side == 1)
											{
												temp.append("q");
											}
											break;
				case ChessPieces::KING :	if(emptySpaces > 0)
											{
												temp.append(std::to_string(emptySpaces));
												emptySpaces = 0;
											}
											if(chessBoard[i][j].side == 0)
											{
												temp.append("K");
											}
											else if(chessBoard[i][j].side == 1)
											{
												temp.append("k");
											}
											break;
				default : break;
			}
		}
		if(emptySpaces != 0)
			{
				temp.append(std::to_string(emptySpaces));
			}
		if(i != 0)
		{
			temp.append("/");
		}
	}
	//First which turn it is (w/b);
	if(!turn)
	{
		temp.append(" w ");
	}
	else
	{
		temp.append(" b ");
	}

	//Castling rights (- for none, K for white kingside, Q for white queenside (kq for black), ordered KQkq)
	if(chessBoard[0][3].castlingRights && wcastle) //White king
	{
		if(chessBoard[0][0].castlingRights)
		{
			temp.append("K");
		}
		if(chessBoard[0][7].castlingRights)
		{
			temp.append("Q");
		}
		if(!(chessBoard[0][0].castlingRights || chessBoard[7][0].castlingRights))
		{
			wcastle = false;
		}
	}
	else
	{
		wcastle = false;
	}

	if(chessBoard[7][3].castlingRights && bcastle) //White king
	{
		if(chessBoard[7][0].castlingRights)
		{
			temp.append("k");
		}
		if(chessBoard[7][7].castlingRights)
		{
			temp.append("q");
		}
		if(!(chessBoard[7][7].castlingRights || chessBoard[7][0].castlingRights))
		{
			bcastle = false;
		}
	}
	else
	{
		bcastle = false;
	}
	if(!(bcastle && wcastle))
	{
		temp.append("-");
	}
	//En passant square (if a e2e4 happens for instance this field should be "e3"), if none "-"
	temp.append(" " + enPassanteTile);
	//halfmove clock (number of moves since last pawn advance or capture, used to determine draw by fifty-move rule)
	temp.append(" " + std::to_string(fiftyMovesCounter));
	//fullmove clock, number of moves done (starts at 1, increments after black moves.)
	moveClock = 1 + int(turnCounter/2);
	temp.append(" " + std::to_string(moveClock));
	return temp;
}




//AI TRAINING
void Logic::aiTrain()
{
	#if defined(__linux__)
	std::cout << "aiTrain" << std::endl;
	//First we want to set how many iterations we want:
	std::string tempString;
	bool isNumber;
	int iterationCount;


	std::cout << "NEAT loves playing with itself! How many times should it play with itself?" << std::endl;
	std::cin >> iterationCount;
	std::cout << "Do you want debug information (Y/N)?" << std::endl;
	std::cin >> tempString;
	if(tempString == "Y" || tempString == "N")
	{
		//Score A and B are for the AI's, scorePrev and scoreCurrent are for calculating the score.
		int scoreA, scoreB, mBlack, mWhite, scorePrev, scoreCurrent;
		float avgA, avgB;
		scoreA = 0;
		scoreB = 0;
		scoreCurrent = 20;
		
		//Used to get score from stockfish
		char buff[10];
		FILE *in;
		std::string returnValue, command;
		gameCount = 0;
		std::cout << "it's playing with itself!" << std::endl;
		//loop through iterations!
		while(gameCount < iterationCount)
		{
			bool turn = true;
			int temp = gameCount;
			
			scoreA = 0;
			scoreB = 0;
			avgA = 0;
			avgB = 0;
			mWhite = 0;
			mBlack = 0;
			while(temp == gameCount)
			{
				
				scorePrev = scoreCurrent;
				//check stockfish for score
				command = "./stockfish.exe position fen " + getFEN();
				if(!(in = popen(command.c_str(), "r")))
				{
					std::cout << "Failed to execute stockfish command!" << std::endl;
				}
				else
				{	
					fgets(buff, sizeof(buff), in);
					returnValue = buff;
					scoreCurrent = std::stoi(returnValue);
					//std::cout << "Stockfish output: " << returnValue << std::endl;
					
					if (tempString == "Y")
					{
						std::cout << "Current moves made: " << stockfishBoardString << std::endl;
						std::cout << "Current FEN string: " << getFEN() << std::endl;
					}
					if(!turn)
					{
						if (tempString == "Y" || tempString == "N")
						{
							//std::cout << "White ScoreModifier:" << scoreCurrent - scorePrev << std::endl;
						}
						scoreA += scoreCurrent - scorePrev;
						//std::cout << "Current score white: " << scoreA << std::endl;
						turn=!turn;
						++mWhite;
					}
					else
					{
						if (tempString == "Y" || tempString == "N")
						{
							//std::cout << "Black ScoreModifier: " << scoreCurrent - scorePrev << std::endl;
						}
						scoreB -= scoreCurrent - scorePrev;
						//std::cout << "Current score black: " << scoreB << std::endl;
						turn=!turn;
						++mBlack;
					}
				}
				pclose(in);
				std::vector<moveInfo> legalMoves = getAllMoves();

				if (turn == 0)
				{
					float inputs[70];
			
					int j = 0;
			
					for (int x = 0; x < chessBoard.size(); ++x)
					{
						for (int y = 0; y < chessBoard[x].size(); ++y, ++j)
						{
							if (chessBoard[x][y].side == turn)
							{
								inputs[j] = chessBoard[x][y].type / 6.0f;
							}
							else
							{
								inputs[j] = -(chessBoard[x][y].type / 6.0f);
							}
						}
					}
			
					for (int i = 0; i < legalMoves.size(); ++i)
					{
						inputs[j] = legalMoves[i].startX / 8.0f;
						inputs[j + 1] = legalMoves[i].startY / 8.0f;
						inputs[j + 2] = legalMoves[i].endX / 8.0f;
						inputs[j + 3] = legalMoves[i].endY / 8.0f;
						inputs[j + 4] = legalMoves[i].typeMoved / 6.0f;
						inputs[j + 5] = -(legalMoves[i].atLocation / 6.0f);
			
						float output[1];
			
						whiteBrain->input(inputs, output);
			
						legalMoves[i].score = output[0];
					}
			
					//printf("Score for first from white: %f\n", legalMoves.at(0).score);
				}
				else
				{
					float inputs[70];
			
					int j = 0;
			
					for (int x = 0; x < chessBoard.size(); ++x)
					{
						for (int y = 0; y < chessBoard[x].size(); ++y, ++j)
						{
							if (chessBoard[x][y].side == turn)
							{
								inputs[j] = chessBoard[x][y].type / 6.0f;
							}
							else
							{
								inputs[j] = -(chessBoard[x][y].type / 6.0f);
							}
						}
					}
			
					for (int i = 0; i < legalMoves.size(); ++i)
					{
						inputs[j] = legalMoves[i].startX / 8.0f;
						inputs[j + 1] = legalMoves[i].startY / 8.0f;
						inputs[j + 2] = legalMoves[i].endX / 8.0f;
						inputs[j + 3] = legalMoves[i].endY / 8.0f;
						inputs[j + 4] = legalMoves[i].typeMoved / 6.0f;
						inputs[j + 5] = -(legalMoves[i].atLocation / 6.0f);
			
						float output[1];
			
						blackBrain->input(inputs, output);
			
						legalMoves[i].score = output[0];
					}
			
					//printf("Score for first from black: %f\n", legalMoves.at(0).score);
				}
			
				moveInfo bestMove = legalMoves.at(0);
				for (int i = 1; i < legalMoves.size(); ++i)
				{
					if (legalMoves[i].score > bestMove.score)
					{
						bestMove = legalMoves[i];
					}
				}
			
				int pieceID = chessBoard[bestMove.startX][bestMove.startY].pieceId;
			
				doLegalMove(pieceID, glm::vec2(bestMove.endX, bestMove.endY), bestMove.typeMoved);
			}
			std::cout << "White score: " << scoreA << "\nBlack score: " << scoreB << std::endl;
			//Log scores to file (too make it easier to record progress)
			//std::ofstream scoreLog("score.log", std::ios_base::app | std::ios_base::out)
			//scoreLog << "Generation x" << scoreA << "\n Generation Y" << scoreB << '\n';

			avgA = (float)scoreA/mWhite;
			avgB = (float)scoreB/mBlack;

			std::ofstream scoreLog("score.log", std::ios_base::app | std::ios_base::out);
			std::ifstream readFromFile("./resources/genomes/config.txt");

			int genomeID, highestScorerGenome;
			float score;

			readFromFile >> genomeID >> score >> highestScorerGenome;

			std::ofstream writeTFile("./resources/genomes/config.txt");


			if (avgA >= avgB)
			{
				scoreLog << ++genomeIDCounter << "\t\t\t" << avgA << '\n';

				if(avgA > score)
				{
					writeTFile << genomeIDCounter << '\n' << avgA << '\t' << genomeIDCounter << "\n\n";
				}
				else
				{
					writeTFile << genomeIDCounter << '\n' << score << '\t' << highestScorerGenome << "\n\n";
				}

				whiteBrain->writeToFile(genomeIDCounter);

				delete blackBrain;
				if(gameCount % 20 == 0)
				{
					blackBrain = new Genome(highestScorerGenome);
				}
				else
				{
					blackBrain = new Genome(whiteBrain);
		
					whiteBrain->mutate();
					blackBrain->mutate();
				}
			}
			else
			{
				scoreLog << ++genomeIDCounter << "\t\t\t" << avgB << '\n';

				if(avgB > score)
				{
					writeTFile << genomeIDCounter << '\n' << avgB << '\t' << genomeIDCounter << "\n\n";
				}
				else
				{
					writeTFile << genomeIDCounter << '\n' << score << '\t' << highestScorerGenome << "\n\n";
				}

				blackBrain->writeToFile(genomeIDCounter);


				delete whiteBrain;
				if(gameCount % 20 == 0)
				{
					whiteBrain = new Genome(highestScorerGenome);
				}
				else
				{
					whiteBrain = new Genome(blackBrain);
	
	
					blackBrain->mutate();
					whiteBrain->mutate();
				}

			}	
			//REFRESH BOARD HERE!!!
			scoreLog.close();
			resetLogic();
		}
	}
	else
	{
		std::cout << "How hard is it to type Y or N?" << std::endl;
	}

	
	#endif
}

void Logic::doLegalMoveTrain(int pieceID, glm::vec2 tileNew, ChessPieces pawnUpgradeType)
{
	pieceMoved = pieceID;
	newLocation = tileNew;
	tile temp2, temp;
	ChessPieces oldPiece = chessBoard.at(tileNew.x).at(tileNew.y).type;
	if (tileNew.x != 500)
	{
		for (size_t i = 0; i < chessBoard.size(); i++)
		{
			for (size_t f = 0; f < chessBoard.at(i).size(); f++)
			{
				if (chessBoard.at(i).at(f).pieceId == pieceID)
				{
					temp2 = chessBoard.at(i).at(f);
					temp.side = -1;
					temp.type = ChessPieces::NOPIECE;
					temp.x = i;
					temp.y = f;
					temp.tileId = temp2.tileId;
					temp.pieceId = -1;
					temp.move = Move::NoneLegalSpace;
					temp.castlingRights = false;
					temp.pawnCounter = -1;
					chessBoard.at(i).at(f) = temp;
				}
			}
		}
		temp2.x = tileNew.x;
		temp2.y = tileNew.y;
		temp2.castlingRights = false;
		temp2.tileId = chessBoard.at(tileNew.x).at(tileNew.y).tileId;
		chessBoard.at(tileNew.x).at(tileNew.y) = temp2;
		if (temp2.type == PAWN)
		{
			if (turn == 0 && temp.x == 1 && temp2.x == 3)	
			{
				enPassanteTile = ' ';
				switch (temp.y)
				{
				case 7: enPassanteTile = 'a'; break;
				case 6: enPassanteTile = 'b'; break;
				case 5: enPassanteTile = 'c'; break;
				case 4: enPassanteTile = 'd'; break;
				case 3: enPassanteTile = 'e'; break;
				case 2: enPassanteTile = 'f'; break;
				case 1: enPassanteTile = 'g'; break;
				case 0: enPassanteTile = 'h'; break;
				}
				enPassanteTile.append(std::to_string(temp.x + 1));
			}
			else if (turn == 1 && temp.x == 6 && temp2.x == 4 )
			{
				enPassanteTile = ' ';
				switch (temp.y)
				{
				case 7: enPassanteTile = 'a'; break;
				case 6: enPassanteTile = 'b'; break;
				case 5: enPassanteTile = 'c'; break;
				case 4: enPassanteTile = 'd'; break;
				case 3: enPassanteTile = 'e'; break;
				case 2: enPassanteTile = 'f'; break;
				case 1: enPassanteTile = 'g'; break;
				case 0: enPassanteTile = 'h'; break;
				}
				enPassanteTile.append(std::to_string(temp.y + 1));
			}
		}
		else
		{
			enPassanteTile = "-";
		}
		if (temp2.type == PAWN && temp.x == 5 && turn == 0)
		{
			if (temp2.y == temp.y + 1 && chessBoard.at(tileNew.x).at(tileNew.y).side == -1)
			{
				chessBoard.at(tileNew.x-1).at(tileNew.y).pieceId = -1;
				chessBoard.at(tileNew.x-1).at(tileNew.y).type = NOPIECE;
			}
			else if (temp2.y == temp.y - 1 && chessBoard.at(tileNew.x).at(tileNew.y).side == -1)
			{
				chessBoard.at(tileNew.x-1).at(tileNew.y).pieceId = -1;
				chessBoard.at(tileNew.x-1).at(tileNew.y).type = NOPIECE;
			}
		}
		if (temp2.type == PAWN && temp.x == 4 && turn == 1)
		{
			if (temp2.y == temp.y + 1 && chessBoard.at(tileNew.x).at(tileNew.y).side == -1)
			{
				chessBoard.at(tileNew.x - 1).at(tileNew.y).pieceId = -1;
				chessBoard.at(tileNew.x - 1).at(tileNew.y).type = NOPIECE;
			}
			else if (temp2.y == temp.y - 1 && chessBoard.at(tileNew.x).at(tileNew.y).side == -1)
			{
				chessBoard.at(tileNew.x - 1).at(tileNew.y).pieceId = -1;
				chessBoard.at(tileNew.x - 1).at(tileNew.y).type = NOPIECE;
			}
		}
		addToStockfishBoardString(glm::vec2(temp.x, temp.y), tileNew);
		if (oldPiece == NOPIECE && temp2.type != PAWN)
		{
			fiftyMovesCounter++;
		}
		else
		{
			fiftyMovesCounter = 0;
		}
		if (chessBoard.at(tileNew.x).at(tileNew.y).type == KING)
		{
			if (temp.x == turn * 7 && temp.y == 3)
			{
				if (tileNew.y == 1)
				{
					temp.x = turn * 7;
					temp.y = 0;

					temp2 = chessBoard.at(turn * 7).at(0);
					temp.tileId = temp2.tileId;

					chessBoard.at(turn * 7).at(0) = temp;

					temp2.x = turn * 7;
					temp2.y = 2;

					temp2.tileId = chessBoard.at(turn * 7).at(2).tileId;
					chessBoard.at(turn * 7).at(2) = temp2;
				}
				if (tileNew.y == 5)
				{
					temp.x = turn * 7;
					temp.y = 7;

					temp2 = chessBoard.at(turn * 7).at(7);
					temp.tileId = temp2.tileId;

					chessBoard.at(turn * 7).at(7) = temp;

					temp2.x = turn * 7;
					temp2.y = 4;
					temp2.tileId = chessBoard.at(turn * 7).at(4).tileId;
					chessBoard.at(turn * 7).at(4) = temp2;
				}
			}
		}
		if (turn == 0 && chessBoard.at(tileNew.x).at(tileNew.y).type == PAWN && tileNew.x == 7)
		{
 			if (pawnUpgradeType != NOPIECE)
			{
				chessBoard.at(tileNew.x).at(tileNew.y).type = pawnUpgradeType;

				if (pawnUpgradeType == QUEEN) { stockfishBoardString.append("q"); }
				if (pawnUpgradeType == KNIGHT) { stockfishBoardString.append("k"); }
				if (pawnUpgradeType == BISHOP) { stockfishBoardString.append("b"); }
				if (pawnUpgradeType == ROOK) { stockfishBoardString.append("r"); }
			}
			else
			{
				this->pawnUpgrade = true;
				pawnToBeUpgraded = tileNew;
			}
			//	std::cout << "im in here\n\n";
		}
		else if (turn == 1 && chessBoard.at(tileNew.x).at(tileNew.y).type == PAWN && tileNew.x == 0)
		{
			if (pawnUpgradeType != NOPIECE)
			{
				chessBoard.at(tileNew.x).at(tileNew.y).type = pawnUpgradeType;

				if (pawnUpgradeType == QUEEN) { stockfishBoardString.append("q"); }
				if (pawnUpgradeType == KNIGHT) { stockfishBoardString.append("k"); }
				if (pawnUpgradeType == BISHOP) { stockfishBoardString.append("b"); }
				if (pawnUpgradeType == ROOK) { stockfishBoardString.append("r"); }
			}
			else
			{
				this->pawnUpgrade = true;
				pawnToBeUpgraded = tileNew;
			}
		}
		if (turn == 0 && chessBoard.at(tileNew.x).at(tileNew.y).type == PAWN && tileNew.x == 3 && temp.x == 1)
		{
			chessBoard.at(tileNew.x).at(tileNew.y).pawnCounter = turnCounter;
		}
		if (turn == 1 && chessBoard.at(tileNew.x).at(tileNew.y).type == PAWN && tileNew.x == 4 && temp.x == 6)
		{
			chessBoard.at(tileNew.x).at(tileNew.y).pawnCounter = turnCounter;
		}
		if (turn == 0)
		{
			turn = 1;
		}
		else
		{
			turn = 0;
		}
		turnCounter++;
		if (checkMateTrain(turn))
		{
			++gameCount;
		}
		//std::cout << stockfishBoardString << std::endl;
		//printf("%s\n", stockfishBoardString.c_str());
	}
}

bool Logic::checkMateTrain(int side)
{
	glm::vec2 piece;
	bool finished = true;
	int otherside;
	reset();
	if (side == 0)
	{
		otherside = 1;
	}
	else
	{
		otherside = 0;
	}
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t f = 0; f < CHESSROW; f++)
		{
			if (chessBoard.at(i).at(f).side == side)
			{
				piece = glm::vec2(i, f);
				tile temp = chessBoard.at(i).at(f);
				switch (temp.type)
				{
				case ChessPieces::PAWN:
					checkPawn(piece, temp);
					break;
				case ChessPieces::ROOK:
					checkRook(piece, temp);
					break;
				case ChessPieces::BISHOP:
					checkBishop(piece, temp);
					break;
				case ChessPieces::KNIGHT:
					checkKnight(piece, temp);
					break;
				case ChessPieces::QUEEN:
					checkQueen(piece, temp);
					break;
				case ChessPieces::KING:
					checkKing(piece, temp);
					break;
				}
			}
		}
	}
	for (size_t i = 0; i < CHESSROW; i++)
	{
		for (size_t f = 0; f < CHESSROW; f++)
		{
			if (chessBoard.at(i).at(f).move == EnemySpace || chessBoard.at(i).at(f).move == EmptySpace)
			{
				//std::cout << i << "x " << f << "y" << chessBoard.at(i).at(f).move << "and" << chessBoard.at(i).at(f).type << std::endl;
				finished = false;
			}
		}
	}

	tile kingTile = findKingTile(side);
	if (check(side) && finished)
	{
		if (turn == 0)
		{
			std::cout << "Black wins!\n";
		}
		else
		{
			std::cout << "White wins!\n";
		}
	}
	else if (finished)
	{
		std::cout << "Stalemate!\n";
	}
	if (turnCounter > MAXTURN)
	{
		std::cout << "Stalemate!\n";
		finished = true;
	}
	if (treeSteps())
	{
		std::cout << "Stalemate!\n";
		finished = true;
	}
	if (fiftyMovesCounter > 50)
	{
		std::cout << "Stalemate!\n";
		finished = true;
	}

	if (finished)
	{
		printf("This shuold be called once per game end\n");

	}


	return finished;
}