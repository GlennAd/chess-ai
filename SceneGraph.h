#pragma once
#include <vector>

#include "Actor.h"
#include "ModelHandler.h"
#include "MoveBlock.h"


struct tile
{
	//follow the tile
	int x, y; // X here is the first row of the vector as int [x][y] so x is numbers while y is letters
	int tileId;
	// Follows the piece
	ChessPieces type;
	int side;		// side is ether -1 = empty, 0 = white, 1 = black
	int pieceId;
	Move move;
	bool castlingRights;
	int pawnCounter;
};

class SceneGraph
{
public:
	void update();
	void update1(std::vector<std::vector<Move>>* moves);
	void updateNew();
	glm::vec2 update2(int piece, int Location);
	void draw(Camera * camera, ShaderHandler::ShaderProgram * shaderProgram, LightHandler * lightHandler);
	void draw2(Camera * camera, ShaderHandler::ShaderProgram * shaderProgram, LightHandler * lightHandler);
	SceneGraph(ModelHandler* modelHandler, std::vector<std::vector<tile>>* chessBoard);
	void pawnUpgrade(int type, int tile);
	void pawnChange(bool change);
	~SceneGraph();
private:
	int findLocation(MoveBlock block);

	void castling(int piece, int Location);
	std::vector<std::vector<Move>> board;
	std::vector<Actor> actors;
	std::vector<Actor> pawnChangeActors;
	std::vector<MoveBlock> moveBlock;
	glm::vec3 boardOffset;
	glm::vec2 closestWhole(glm::vec2 number);
	std::vector<std::vector<tile>> *chessBoard;
	ModelHandler* modelHandler;

	const glm::vec3 scale = glm::vec3(15.0f);
	double tileHight, tileLength;
	bool drawChange = true;
};
