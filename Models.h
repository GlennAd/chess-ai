#pragma once

#if defined(__linux__)						// If we are using linux.
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#endif

#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>

#include "Mesh.h"
#include "TextureHandler.h"
#include "Camera.h"
#include "ShaderHandler.h"
#include "LightHandler.h"

class Models
{
public:
	Models();
	Models(std::string filePath, TextureHandler* textureHandler);
	void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler, glm::mat4* modelMatrix, Color color, int id);
	glm::vec3 size();
	glm::vec3 sizeDiffrence();
private:
	//--------------------------------PRIVATE VARIABLES----------------------------------
	TextureHandler* textureHandler;
	std::vector<Texture> texturesLoaded;
	std::vector<Mesh> meshes;
	std::string directory;

	//--------------------------------PRIVATE FUNCTIONS----------------------------------
	void loadModel(std::string path);
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTexture(aiMaterial* mat, aiTextureType type, std::string typeName);
};

//lots of inspiration from http://learnopengl.com/#!Model-Loading/Assimp