#include "LightHandler.h"

//Creates a light at the player
LightHandler::LightHandler(glm::vec3 Playerpos)
{
	light.linear = 0.0045f;
	light.quadratic = 0.00045f;
	light.position = Playerpos;
	light.diffuse = glm::vec3(1.0f, 1.0f, 0.0f);
	light.specular = glm::vec3(1.0f, 1.0f, 1.0f);
	light.direction = glm::vec3(0.0f, 0.0f, 0.0f);
	light.cutoff = 0.8f;
	light.outerCutoff = 0.4f;
	lightSwitch = true;
}

//updates position and direction of light
void LightHandler::update(glm::vec3 pos, glm::vec3 dir)
{
	light.position = pos + glm::vec3(0.2f, 0.7f, 0.2f);
	light.direction = dir;
}

//Swaps between spotlight and pointlight
void LightHandler::switchLight()
{
	lightSwitch = !lightSwitch;
}

//Creates the projection matrices for the lights
void LightHandler::createProjectionMatrixes(Framebuffer& framebuffer, std::vector<glm::mat4> shadowTransforms, ShaderHandler::ShaderProgram& shaderProgram, const GLfloat& projectionFar)
{
	const GLfloat aspect = 1080 / 720;
	const GLfloat projectionNear = 0.1f;

	//Projection matrix is the same for the whole cubemap
	glm::mat4 shadowProjection = glm::perspective(glm::radians(90.0f), aspect, projectionNear, projectionFar);

	//View matrixes for the 6 sides of the cubemap
	shadowTransforms.push_back(shadowProjection *
		glm::lookAt(light.position, light.position + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProjection *
		glm::lookAt(light.position, light.position + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProjection *
		glm::lookAt(light.position, light.position + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
	shadowTransforms.push_back(shadowProjection *
		glm::lookAt(light.position, light.position + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
	shadowTransforms.push_back(shadowProjection *
		glm::lookAt(light.position, light.position + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProjection *
		glm::lookAt(light.position, light.position + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));

	for (int i = 0; i < 6; ++i)
	{
		//Uniform for each matrix
		glUniformMatrix4fv(glGetUniformLocation(shaderProgram.programId, ("shadowMatrices[" + std::to_string(i) + "]").c_str()), 1, GL_FALSE, glm::value_ptr(shadowTransforms.at(i)));
	}
	glUniform3fv(shaderProgram.lighPosID, 1, &light.position[0]);
}

//---------------------------------------GET FUNCTIONS-----------------------------
Light LightHandler::getLight()
{
	return light;
}

bool LightHandler::getLightSwitch()
{
	return lightSwitch;
}