#include "Node.h"
#include "Config.h"

//Node::Node(int column, sf::Vector2f& offset)
Node::Node(int column)
{
	//this->offset = offset;

	this->column = column;

	//shape = sf::CircleShape(10.0f);
	//shape.setPosition(offset.x + column * lengthBetweenNodes, offset.y + lengthBetweenNodes);
	//shape.setOrigin(5.0f, 5.0f);
	//shape.setFillColor(sf::Color(0, 255, 255, 255));
}

Node::Node(Node* node)
{
	//offset = node->offset;
	column = node->column;

	//shape = node->shape;
}

Node::~Node()
{
	std::vector<Link*>::iterator linkIterator;

	for (linkIterator = outLinks.begin(); linkIterator != outLinks.end(); ++linkIterator)
	{
		delete (*linkIterator);
	}
}


void Node::processInfo()
{
	std::vector<Link*>::iterator linkIterator;

	float inputs = 0.0f;

	for (linkIterator = inLinks.begin(); linkIterator != inLinks.end(); ++linkIterator)
	{
		inputs += (*linkIterator)->value;	
	}

	float output;
	
	//Using TanH activation function
	output = (2 / (1 + pow(2.718128, -2.0f * inputs))) - 1.0f;

	//Using Linear activation function
	//if (inLinks.size() != 0)
	//{
	//	output = inputs / inLinks.size();
	//}
	//else
	//{
	//	output = 0.0f;
	//}

	for (linkIterator = outLinks.begin(); linkIterator != outLinks.end(); ++linkIterator)
	{
		(*linkIterator)->value = output * (*linkIterator)->weight;
	}
}

float Node::processOutputValue()
{
	std::vector<Link*>::iterator linkIterator;

	float inputs = 0.0f;

	for (linkIterator = inLinks.begin(); linkIterator != inLinks.end(); ++linkIterator)
	{
		inputs += (*linkIterator)->value;
	}

	//Using TanH activation function
	return (2 / (1 + pow(2.718128, -2.0f * inputs))) - 1.0f;
}

void Node::addInputLink(Link * link)
{
	inLinks.push_back(link);
}

void Node::addOutputLink(Link * link)
{
	outLinks.push_back(link);
}

std::vector<Link*> Node::deleteOutPutLinks()
{
	std::vector<Link*>::iterator linkIterator;
	std::vector<Link*> linksDeleted;

	for (linkIterator = outLinks.begin(); linkIterator != outLinks.end(); ++linkIterator)
	{
		linksDeleted.push_back((*linkIterator));
		delete (*linkIterator);
	}

	outLinks.clear();

	return linksDeleted;
}

std::vector<Link*>& Node::getInLinks()
{
	return inLinks;
}

std::vector<Link*>& Node::getOutLinks()
{
	return outLinks;
}

/*void Node::draw(sf::RenderWindow & window)
{
	shape.setPosition(offset.x + column * lengthBetweenNodes, shape.getPosition().y);

	window.draw(shape);
}*/
