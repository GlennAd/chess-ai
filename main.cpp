#if defined(__linux__)
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
//Windows
#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#include <SDKDDKVer.h>

#include <SDL2/SDL.h>
#include <gl\glew.h>
#include <SDL2/SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//C++ 11 stuff to use
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <string>
#include <queue>
#include <fstream>
#include <sstream>
#include <chrono>
#include <ctime>

#include "InputHandler.h"
#include "ModelHandler.h"
#include "TextureHandler.h"
#include "SceneGraph.h"
#include "Framebuffer.h"
#include "Logic.h"

#ifdef main
#undef main	
#endif //  main

#define SCREEN_WIDTH 1080
#define SCREEN_HEIGHT 720
struct MouseClick
{
	bool DevelopmentMouseMovement = false;
	int x, y;
	bool colorPicking = false;
	bool actorHasBeenPicked = false;
	bool actorsHasBeenUpdated = false;
	bool PlaceHasBeenPicked = false;
	int actorPicked = 0;
	int placePicked = 0;
	int lastPicked = 0;
};

std::string windowName = "Vaporwave: Chess"; //You should update this to your name

SDL_Window* gWindow = NULL;

std::vector<std::vector<Move>> moves;

SDL_GLContext gContext;

bool initGL();

bool graphicInit()
{
	bool success = true;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else {
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

		SDL_SetRelativeMouseMode(SDL_FALSE);
		gWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else {
			//Create context
			gContext = SDL_GL_CreateContext(gWindow);

			if (gContext == NULL) {
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else {
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK) {
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}
				if (SDL_GL_SetSwapInterval(1) < 0) {
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}
				if (!initGL()) {
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}
			}
		}
	}
	return success;
}
bool initGL()
{

	bool success = true;
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_MULTISAMPLE);
	
	glDepthFunc(GL_LESS);
	
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	return success;
}

void update(float deltaTime, std::queue<GameEvent>& eventQueue, Camera* camera, LightHandler* lightHandler, MouseClick* mouseClick, SceneGraph *sceneGraph, Logic *logic)
{
	while (!eventQueue.empty())
	{
	//	sceneGraph->update();
		GameEvent nextEvent = eventQueue.front();
		eventQueue.pop();
		if (nextEvent.action == ActionEnum::LEFT)
		{
			glm::vec3 tempPos = camera->getPos();
			camera->setPosition(tempPos - glm::normalize(glm::cross(camera->getFront(), camera->getUp())));
			std::cout << tempPos.x << "x " << tempPos.y << "y " << tempPos.z << "z \n";
		}
		else if (nextEvent.action == ActionEnum::RIGHT)
		{
			glm::vec3 tempPos = camera->getPos();
			camera->setPosition(tempPos + glm::normalize(glm::cross(camera->getFront(), camera->getUp())));
			std::cout << tempPos.x << "x " << tempPos.y << "y " << tempPos.z << "z \n";
		}
		else if (nextEvent.action == ActionEnum::FORWARD)
		{
			glm::vec3 tempPos = camera->getPos();
			camera->setPosition(tempPos + camera->getFront());
			std::cout << tempPos.x << "x " << tempPos.y << "y " << tempPos.z << "z \n";
		}
		else if (nextEvent.action == ActionEnum::BACK)
		{
			glm::vec3 tempPos = camera->getPos();
			camera->setPosition(tempPos - camera->getFront());
			std::cout << tempPos.x << "x " << tempPos.y << "y " << tempPos.z << "z \n";
		}
		else if (nextEvent.action == ActionEnum::MOUSEMOTION && mouseClick->DevelopmentMouseMovement)
		{
			int x, y;
			SDL_GetRelativeMouseState(&x, &y);
			//std::cout << x << "x " << y << "y " << std::endl;
			glm::vec2 temp;
			temp.x = x;
			temp.y = y;
			camera->lookAround(temp);
		}
		else if (nextEvent.action == ActionEnum::SPACE)
		{
			mouseClick->DevelopmentMouseMovement = !mouseClick->DevelopmentMouseMovement;
			if (mouseClick->DevelopmentMouseMovement)
			{
				SDL_SetRelativeMouseMode(SDL_TRUE);
			}
			else
			{
				SDL_SetRelativeMouseMode(SDL_FALSE);
			}
		}
		else if (nextEvent.action == ActionEnum::LEFTMOUSEBUTTON && !mouseClick->DevelopmentMouseMovement && !mouseClick->actorHasBeenPicked)
		{
			SDL_GetMouseState(&mouseClick->x, &mouseClick->y);
			//	std::cout << mouseClick->x << "x " << mouseClick->y << "y " << std::endl;

			mouseClick->colorPicking = true;
		}
		else if (nextEvent.action == ActionEnum::RIGHTMOUSEBUTTON && !mouseClick->DevelopmentMouseMovement && mouseClick->actorHasBeenPicked)
		{
			SDL_GetMouseState(&mouseClick->x, &mouseClick->y);
			//	std::cout << mouseClick->x << "x " << mouseClick->y << "y " << std::endl;
			mouseClick->colorPicking = true;
		}
	}
	if (mouseClick->actorHasBeenPicked && !mouseClick->actorsHasBeenUpdated)
	{
		//std::cout << "Actor is " << mouseClick->actorPicked << std::endl;
		if (mouseClick->actorPicked < 32 && !logic->getPawnUpgrade()) // number of pieces
		{
			
			if (logic->checkForLegalMoves(mouseClick->actorPicked))
			{
				auto t = logic->LegalMoves();
				sceneGraph->update1(&t);
				mouseClick->actorsHasBeenUpdated = true;
				
			}
			else
			{
				mouseClick->actorHasBeenPicked = false;
			}				
		}
		else
		{
			mouseClick->actorHasBeenPicked = false;
		}
		if (mouseClick->actorPicked >= 100 && logic->getPawnUpgrade())
		{
		
			logic->upgradePawn(mouseClick->actorPicked);
			sceneGraph->pawnUpgrade(mouseClick->actorPicked, mouseClick->lastPicked);
		}
	}
	if (mouseClick->PlaceHasBeenPicked)
	{
		//logic move and tile move
		//std::cout << "tile is " << mouseClick->placePicked << std::endl;
		logic->doLegalMove(mouseClick->actorPicked, sceneGraph->update2(mouseClick->actorPicked, mouseClick->placePicked));
		mouseClick->PlaceHasBeenPicked = false;
		mouseClick->lastPicked = mouseClick->actorPicked;
		
	}

	logic->aiMove();
	//int i;
	//int temp;
	//logic->getScreenInfo(i, temp);
	//
	//sceneGraph->update2(i, temp);
	//sceneGraph->pawnChange(logic->getPawnUpgrade());
	sceneGraph->updateNew();
	camera->lookAt();
	lightHandler->update(glm::vec3(80.0f, 0.0f, 80.0f), glm::vec3(0));
}

void draw(Camera* camera, ShaderHandler* shaderHandler, LightHandler* lightHandler, SceneGraph* sceneGraph, Framebuffer* frameBuffer, MouseClick* mouseClick)
{
	ShaderHandler::ShaderProgram* shaderProgram;
	if (mouseClick->colorPicking)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		shaderProgram = shaderHandler->getShader("color");
		sceneGraph->draw(camera, shaderProgram, lightHandler);
		glBindFramebuffer(GL_PIXEL_PACK_BUFFER, frameBuffer->frameBufferObject);
		glFlush();
		glFinish();

		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		float data[4];
		data[0] = 0;
		glReadPixels(mouseClick->x, SCREEN_HEIGHT - mouseClick->y, 1, 1, GL_RGBA, GL_FLOAT, data);
		//std::cout << mouseClick->x << "x " << mouseClick->y << "y " << std::endl;
		//std::cout << "data[0] =  " << data[0] * 255 << std::endl;
		if (mouseClick->actorHasBeenPicked)
		{
			mouseClick->placePicked = data[0] * 255;
			mouseClick->PlaceHasBeenPicked = true;
			mouseClick->actorHasBeenPicked = false;
		}
		else
		{
			mouseClick->actorPicked = data[0] * 255;
			mouseClick->actorHasBeenPicked = true;
			mouseClick->actorsHasBeenUpdated = false;
		}
		glClear(GL_PIXEL_PACK_BUFFER);

		glBindFramebuffer(GL_PIXEL_PACK_BUFFER, 0);
		mouseClick->colorPicking = false;
	}
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shaderProgram = shaderHandler->getShader("pointLight");

	glUseProgram(shaderProgram->programId);

	sceneGraph->draw2(camera, shaderProgram, lightHandler);

	SDL_GL_SwapWindow(gWindow);
}









int main()
{
	std::string menuSelection;

	do
	{
		std::cout 	<< "\nSelect mode:\n"
					<< "\t1. NEAT plays with itself (Without Grapics) #Training.\n"
					<< "\t2. NEAT plays with itself (With Grapic)\n"
					<< "\t3. Cancel/Quit\n"
					<< "Selection: ";

		std::cin >> menuSelection;

		if (menuSelection == "1")
		{

			//Not sure if it works on windows or not (hence the "#if defined(__linux__)"), but it sure as hell works on Linux! :D
			//if it doesn't work on windows Glenn can use whatever way he wants to make it work ;)
			//Might help: http://stackoverflow.com/questions/450865/what-is-the-equivalent-to-posix-popen-in-the-win32-api
			#if defined(__linux__)
			
			std::cout << "selection 1!" << std::endl;
			Logic logic;
			logic.aiTrain();
			/*std::cout << "FEN string: " << logic.getFEN() << std::endl;

			//First we want to set how many iterations we want:
			std::string tempString;
			bool isNumber;
			int iterationCount;


			std::cout << "NEAT loves playing with itself! How many times should it play with itself?" << std::endl;
			std::cin >> iterationCount;

			std::cout << "Do you want debug information (Y/N)?" << std::endl;
			std::cin >> tempString;
			if(tempString == "Y" || tempString == "N")
			{
				//Score A and B are for the AI's, scorePrev and scoreCurrent are for calculating the score.
				int scoreA, scoreB, scorePrev, scoreCurrent;
				scoreA = 0;
				scoreB = 0;
				scoreCurrent = 20;
				
				//Used to get score from stockfish
				char buff[10];
				FILE *in;
				std::string returnValue, command;

				logic.setGameCount(0);
				std::cout << "it's playing with itself!" << std::endl;
				//loop through iterations!
				while(logic.getGameCount() < iterationCount)
				{
					bool turn = true;
					int temp = logic.getGameCount();
					
					scoreA = 0;
					scoreB = 0;

					while(temp == logic.getGameCount())
					{
						


						scorePrev = scoreCurrent;
						//check stockfish for score
						command = "./stockfish.exe position fen " + logic.getFEN();
						if(!(in = popen(command.c_str(), "r")))
						{
							std::cout << "Failed to execute stockfish command!" << std::endl;
						}
						else
						{	
							fgets(buff, sizeof(buff), in);
							returnValue = buff;
							scoreCurrent = std::stoi(returnValue);
							std::cout << "Stockfish output: " << returnValue << std::endl;
							
							if (tempString == "Y")
							{
								std::cout << "Current moves made: " << logic.getStockfishString() << std::endl;
								std::cout << "Current FEN string: " << logic.getFEN() << std::endl;
							}

							if(turn)
							{
								if (tempString == "Y" || tempString == "N")
								{
									std::cout << "White ScoreModifier:" << scoreCurrent - scorePrev << std::endl;
								}
								scoreA += scoreCurrent - scorePrev;
								std::cout << "Current score white: " << scoreA << std::endl;
								turn=!turn;
							}
							else
							{
								if (tempString == "Y" || tempString == "N")
								{
									std::cout << "Black ScoreModifier: " << scoreCurrent - scorePrev << std::endl;
								}
								scoreB -= scoreCurrent - scorePrev;
								std::cout << "Current score black: " << scoreB << std::endl;
								turn=!turn;
							}
						}
						pclose(in);
						logic.aiMove(scoreA, scoreB);
					}
					std::cout << "White score: " << scoreA << "\nBlack score: " << scoreB << std::endl;

					//Log scores to file (too make it easier to record progress)
					//std::ofstream scoreLog("score.log", std::ios_base::app | std::ios_base::out)
					//scoreLog << "Generation x" << scoreA << "\n Generation Y" << scoreB << '\n';
				}
			}
			else
			{
				std::cout << "How hard is it to type Y or N?" << std::endl;
			}*/


			//used for first testing!
			/*std::string testString = "e2e4 d7d5 e4d5 e7e6 d5e6 f7f6 e6e7 e8f7 e7d8q";
			testString = "./stockfish.exe position startpos moves " + testString;

			char buff[10];

			FILE *in;
			std::string returnValue;

			if(!(in = popen(testString.c_str(), "r")))
			{
				std::cout << "Test failed";
			}
			else
			{
				fgets(buff, sizeof(buff), in);
				returnValue = buff;
				std::cout << returnValue << " \n\n THE RETURN VALUE TRULY IS: " << returnValue << std::endl;
			}*/

			#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
			Logic logic;
			std::cout << logic.getFEN() << std::endl;

			std::cout << "This function only works on Linux as of now." << std::endl;
			#endif
		}
		else if (menuSelection == "2")
		{
			std::cout << "Menu selection 2" << std::endl;
			//main var
			bool running = true;

			//time var
			float fpsGoal = 60.0f;
			float nextFrame = 1 / fpsGoal;	//Time between frames in seconds
			float nextFrameTimer = 0.0f;
			float deltaTime = 0.0f;
			auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
			auto clockStop = clockStart;
			ShaderHandler::ShaderProgram* currentShaderProgram = nullptr;

			//graphic init
			if (!graphicInit())
			{
				std::cout << "Somebody fucked up!!!\n";
			}
			Camera mainCamera(glm::vec3(60.0f, 110.0f, 0.0f));
			mainCamera.setProjection(0.1f, 5000);
			mainCamera.setFront(glm::vec3(0.0f, -1.0f, 0.5f));
			mainCamera.lookAt(glm::vec3(0.0f, -10.0f, 0.0f), glm::vec3(0.0f, -7.0f, -3.0f));
			//texturehandler
			TextureHandler textureHandler;
			//load modelhandler and models
			ModelHandler modelHandler;
			modelHandler.loadModelData("Pawn", "./resources/models/pawn.obj", &textureHandler);
			modelHandler.loadModelData("Knight", "./resources/models/knight.obj", &textureHandler);
			modelHandler.loadModelData("Bishop", "./resources/models/bishop.obj", &textureHandler);
			modelHandler.loadModelData("Rook", "./resources/models/rook.obj", &textureHandler);
			modelHandler.loadModelData("Queen", "./resources/models/queen.obj", &textureHandler);
			modelHandler.loadModelData("King", "./resources/models/king.obj", &textureHandler);
			modelHandler.loadModelData("Board", "./resources/models/board.obj", &textureHandler);
			modelHandler.loadModelData("MoveBlock", "./resources/models/moveblock.obj", &textureHandler);

			

			ShaderHandler shaderHandler;
			currentShaderProgram = shaderHandler.initializeShaders();

			LightHandler lightHandler(glm::vec3(0.0f, 0.0f, 0.0f));

			Framebuffer depthBuffer(SCREEN_WIDTH, SCREEN_HEIGHT, NORMAL);
			//logic init
			Logic logic;
			MouseClick mouseClick;
			//interface init
			SDL_Event eventHandler;
			InputHandler inputs;
			SceneGraph sceneGraph(&modelHandler, logic.getChessBoard());
			std::queue<GameEvent> eventQueue;
			//game loop
			while (running)
			{
				clockStart = std::chrono::high_resolution_clock::now();
				running = inputs.processEvents(eventHandler, eventQueue);
				update(deltaTime, eventQueue, &mainCamera, &lightHandler, &mouseClick, &sceneGraph, &logic);
				//graphic
				//this is a useless comment!
				//logic
				if (nextFrameTimer >= nextFrame)
				{
					draw(&mainCamera, &shaderHandler, &lightHandler, &sceneGraph, &depthBuffer, &mouseClick);
					nextFrameTimer = 0.0f;
				}
				//interface

				clockStop = std::chrono::high_resolution_clock::now();
				deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
				nextFrameTimer += deltaTime;
			}
		}
		else if (menuSelection != "3")
		{
			std::cout << "\nCommand not recognized!" << std::endl;
		}
	}while (menuSelection != "3");

	return 0;
}