#include "RayCasting.h"



RayCasting::RayCasting(Camera* camera, int screenWidth, int screenHight)
{
	this->camera = camera;
	this->screenWidth = screenWidth;
	this->screenHight = screenHight;

}


RayCasting::~RayCasting()
{
}

glm::vec3 RayCasting::mouseClick()
{
	glm::vec3 pos, norm;
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);

	norm.x = (2.0f * x) / screenWidth - 1.0f;
	norm.y = 1.0f - (2.0f * y) / screenHight;
	norm.z = 1.0f;
	glm::vec4 ray_clip = glm::vec4(norm.x, norm.y, -1.0, 1.0);

	glm::vec4 ray_eye = glm::inverse(camera->getProjectionMatrix()) * ray_clip;
	ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0f, 0.0f);
	glm::vec4 temp = (glm::inverse(camera->getViewMatrix()) * ray_eye);
	glm::vec3 ray_wor = glm::vec3(temp.x, temp.y, temp.z);
	// don't forget to normalise the vector at some point
	ray_wor = glm::normalize(ray_wor);
	
	return pos;
}
