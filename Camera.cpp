#include "Camera.h"


//Creates the camera with a position
Camera::Camera(glm::vec3 position)
{
	this->position = position;
	lookAt();
	setProjection(0.1f, 200.0f);
	firstPerson = false;
}

/**
 * Sets the viewMatrix for the camera
 * @param position 	The position the camera should look at
 * @param up  		The up direction
 */
void Camera::lookAt(glm::vec3 position, glm::vec3 front)
{
	if (firstPerson)
	{
		viewMatrix = glm::lookAt(position, position + front, up);
	}
	else
	{
		viewMatrix = glm::lookAt(this->position, this->position + this->front, up);
	}
}

//Shifts between 1st person and 3rd person </3
void Camera::toggleFirstPerson()
{
	firstPerson = !firstPerson;
}

//------------------------------GET FUNCTIONS---------------------------------------
glm::vec3 Camera::getPos()
{
	return position;
}

glm::vec3 Camera::getUp()
{
	return up;
}

glm::vec3 Camera::getFront()
{
	return front;
}

glm::mat4 Camera::getViewMatrix()
{
	return viewMatrix;
}

glm::mat4 Camera::getProjectionMatrix()
{
	return projectionMatrix;
}

bool Camera::getFirstPerson()
{
	return firstPerson;
}

//------------------------------SET FUNCTIONS---------------------------------------
void Camera::setFront(glm::vec3 front)
{
	this->front = front;
}

/**
* Sets the position of the camera
* @param position
*/
void Camera::setPosition(glm::vec3 position)
{
	this->position = position;
}

/**
* Sets the perspective matrix for the camera
* @param near Near clip plane
* @param far  Far clip plane
* @param fov  Field of view angle in radians
*/
void Camera::setProjection(float cameraNear, float cameraFar, float fov)
{
	projectionMatrix = glm::perspective(fov, 1080.0f / 720.0f, cameraNear, cameraFar);
}

void Camera::lookAround(glm::vec2 pos)
{
	glm::vec2 offset;
	

	offset.x = pos.x - last.x;
	offset.y = last.y - pos.y;


	float sensitivity = 0.05;
	offset.x *= sensitivity;
	offset.y *= sensitivity;
	
	yaw += offset.x;
	pitch += offset.y;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;

	glm::vec3 frontTemp;
	frontTemp.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	frontTemp.y = sin(glm::radians(pitch));
	frontTemp.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	front = glm::normalize(frontTemp);
}
