#include "Gene.h"
#include "Config.h"

#include <random>
#include <fstream>

Gene::Gene(int row, int column)
{
	this->column = column;

	//offset = sf::Vector2f(column * lengthBetweenGenes + 100, row * lengthBetweenGenes);

	//Node* inNode = new Node(0, offset);
	//Node* outNode = new Node(1, offset);
	Node* inNode = new Node(0);
	Node* outNode = new Node(1);

	geneLength = 2;

	nodes.push_back(inNode);
	nodes.push_back(outNode);

	inputNodes.push_back(inNode);
	outputNodes.push_back(outNode);
	
	Link* link = new Link(inNode, outNode);

	links.push_back(link);
}

Gene::Gene(std::string filePath, int row, int column)
{
	//std::ifstream file(filePath + "/config.txt");
	std::ifstream file(filePath);

	//amount of columns
	int input;
	file >> input;
	geneLength = input;

	int numberOfNodes;
	
	//offset = sf::Vector2f(column * lengthBetweenGenes + 100, row * lengthBetweenGenes);

	file >> numberOfNodes;


	for (int j = 0; j < numberOfNodes; ++j)
	{
		int column;

		file >> column;

		int yOffset = 0;

		for (int i = 0; i < nodes.size(); ++i)
		{
			if (nodes[i]->column == column)
			{
				++yOffset;
			}
		}

		//Node* node = new Node(column, sf::Vector2f(offset.x, offset.y + yOffset * lengthBetweenNodes));
		Node* node = new Node(column);

		nodes.push_back(node);

		if (column == 0)
		{
			inputNodes.push_back(node);
		}
		else if (column == geneLength - 1)
		{
			outputNodes.push_back(node);
		}
	}


	file >> input;

	int inNode, outNode;
	float weight;

	for (int i = 0; i < input; ++i)
	{
		file >> inNode >> outNode >> weight;

		links.push_back(new Link(nodes[inNode], nodes[outNode], weight));
	}
}

void Gene::writeToFile(std::string filePath)
{
	std::ofstream file(filePath);

	file << geneLength << '\n';
	file << nodes.size() << '\n';

	for (int i = 0; i < nodes.size(); ++i)
	{
		file << nodes[i]->column << ' ';
	}

	file << '\n';

	file << links.size() << '\n';

	for (int i = 0; i < links.size(); ++i)
	{
		int inNode, outNode;

		for (int j = 0; j < nodes.size(); ++j)
		{
			if (links[i]->inNode == nodes[j])
			{
				inNode = j;
			}

			if (links[i]->outNode == nodes[j])
			{
				outNode = j;
			}

		}
		
		file << inNode << ' ' << outNode << ' ' << links[i]->weight << '\n';
	}
}

Gene::Gene(Gene* gene)
{
	column = gene->column;

	//offset = gene->offset;

	geneLength = gene->geneLength;

	generation = gene->generation;

	std::vector<Node*>::iterator nodeIterator;
	std::vector<Node*>::iterator secondNodeIterator;
	std::vector<Link*>::iterator linkIterator;
	int i = 0, j = 0;

	nodes.clear();
	links.clear();
	inputNodes.clear();
	outputNodes.clear();

	for (nodeIterator = gene->nodes.begin(); nodeIterator != gene->nodes.end(); ++nodeIterator)
	{
		nodes.push_back(new Node(*nodeIterator));
	}
	
	for (nodeIterator = gene->inputNodes.begin(); nodeIterator != gene->inputNodes.end(); ++nodeIterator)
	{
		i = 0;

		for (secondNodeIterator = gene->nodes.begin(); secondNodeIterator != gene->nodes.end(); ++secondNodeIterator, ++i)
		{
			if ((*nodeIterator) == (*secondNodeIterator))
			{
				inputNodes.push_back(nodes[i]);
			}
		}
	}

	for (nodeIterator = gene->outputNodes.begin(); nodeIterator != gene->outputNodes.end(); ++nodeIterator)
	{
		i = 0;

		for (secondNodeIterator = gene->nodes.begin(); secondNodeIterator != gene->nodes.end(); ++secondNodeIterator, ++i)
		{
			if ((*nodeIterator) == (*secondNodeIterator))
			{
				outputNodes.push_back(nodes[i]);
			}
		}
	}

	for (linkIterator = gene->links.begin(); linkIterator != gene->links.end(); ++linkIterator)
	{
		i = 0;
		for (nodeIterator = gene->nodes.begin(); nodeIterator != gene->nodes.end(); ++nodeIterator, ++i)
		{
			j = 0;

			for (secondNodeIterator = gene->nodes.begin(); secondNodeIterator != gene->nodes.end(); ++secondNodeIterator, ++j)
			{
				if ((*linkIterator)->inNode == (*nodeIterator) && (*linkIterator)->outNode == (*secondNodeIterator))
				{
					links.push_back(new Link(nodes[i], nodes[j], (*linkIterator)->weight));
				}
			}
		}
	}
}


Gene::~Gene()
{
	std::vector<Node*>::iterator nodeIterator;

	for (nodeIterator = nodes.begin(); nodeIterator != nodes.end(); ++nodeIterator)
	{
		delete (*nodeIterator);
	}
}

int Gene::mutate()
{
	int returnValue = 0;

	std::vector<Link*>::iterator linkIterator;
	std::vector<Link*>::iterator secondLinkIterator;

	std::vector<Node*>::iterator nodeIterator;

	//Young genes mutates more often.
	float ageModifier;
	bool addedInputOrOutputNodes = false;
	
	if (generation < ageModifierLimit)
	{
		ageModifier	= 2.0f - ((float)generation / ageModifierLimit);
	}
	else
	{
		ageModifier = 1.0f;
	}


	for (linkIterator = links.begin(); linkIterator != links.end(); )
	{
		if (percentageDistributor(randomGen) < deleteLinkMutation * ageModifier)
		{
			if ((*linkIterator)->inNode->getOutLinks().size() > 1 && (*linkIterator)->outNode->getInLinks().size() > 1)
			{
				for (secondLinkIterator = (*linkIterator)->inNode->getOutLinks().begin(); secondLinkIterator != (*linkIterator)->inNode->getOutLinks().end(); ++secondLinkIterator)
				{
					if ((*secondLinkIterator) == (*linkIterator))
					{
						(*linkIterator)->inNode->getOutLinks().erase(secondLinkIterator);
						break;
					}
				}

				for (secondLinkIterator = (*linkIterator)->outNode->getInLinks().begin(); secondLinkIterator != (*linkIterator)->outNode->getInLinks().end(); ++secondLinkIterator)
				{
					if ((*secondLinkIterator) == (*linkIterator))
					{
						(*linkIterator)->outNode->getInLinks().erase(secondLinkIterator);
						break;
					}
				}

				delete (*linkIterator);
				linkIterator = links.erase(linkIterator);
			}
			else
			{
				++linkIterator;
			}
		}
		else
		{
			++linkIterator;
		}
	}

	//Mutates links
	for (linkIterator = links.begin(); linkIterator != links.end(); ++linkIterator)
	{
		if (percentageDistributor(randomGen) < weightChangeMutation * ageModifier)
		{
			(*linkIterator)->mutate();
		}
	}

	for (nodeIterator = nodes.begin(); nodeIterator != nodes.end(); ++nodeIterator)
	{
		if (percentageDistributor(randomGen) < newLinkMutation * ageModifier)
		{
			if ((*nodeIterator)->column == 0)
			{
				makeRandomOutLink((*nodeIterator));
			}
			else if ((*nodeIterator)->column == geneLength - 1)
			{
				makeRandomInLink((*nodeIterator));
			}
			else if ((int)percentageDistributor(randomGen) % 2 == 0)
			{
				makeRandomInLink((*nodeIterator));
			}
			else
			{
				makeRandomOutLink((*nodeIterator));
			}
		}
	}

	/*if (percentageDistributor(randomGen) < deleteNodeMutation * ageModifier)
	{
		std::vector<Node*>::iterator currNode;
		std::vector<Link*>::iterator currLink;
		std::vector<Link*>::iterator secondCurrLink;
	
		std::uniform_int_distribution<int> nodePicker(0, nodes.size() - 1);
		int choice = nodePicker(randomGen);
	
		if (!(nodes[choice]->column == 0 && inputNodes.size() == 1) &&
			!(nodes[choice]->column == geneLength - 1 && outputNodes.size() == 1))
		{
			for (linkIterator = nodes[choice]->getInLinks().begin(); linkIterator != nodes[choice]->getInLinks().end(); ++linkIterator)
			{
				if ((*linkIterator)->inNode->getOutLinks().size() == 1)
				{
					break;
				}
			}

			if (linkIterator == nodes[choice]->getInLinks().end())
			{
				for (linkIterator = nodes[choice]->getOutLinks().begin(); linkIterator != nodes[choice]->getOutLinks().end(); ++linkIterator)
				{
					if ((*linkIterator)->outNode->getInLinks().size() == 1)
					{
						break;
					}
				}

				if (linkIterator == nodes[choice]->getOutLinks().end())
				{
					bool lastNodeInColumn = true;
					std::vector<Node*> nodesToMove;

					for (currNode = nodes.begin(); currNode != nodes.end(); ++currNode)
					{
						if ((*currNode)->column == nodes[choice]->column && (*currNode) != nodes[choice])
						{
							lastNodeInColumn = false;
							nodesToMove.push_back((*currNode));
						}
					}

					for (int i = 0; i < nodesToMove.size(); ++i)
					{
						nodesToMove[i]->shape.setPosition(nodesToMove[i]->shape.getPosition().x, offset.y + (i + 1) * lengthBetweenNodes);
					}
	
					if (lastNodeInColumn)
					{
						for (currNode = nodes.begin(); currNode != nodes.end(); ++currNode)
						{
							if ((*currNode)->column > nodes[choice]->column)
							{
								--(*currNode)->column;
							}
						}
	
						--geneLength;
					}
	
					for (currLink = nodes[choice]->getInLinks().begin(); currLink != nodes[choice]->getInLinks().end(); ++currLink)
					{
						for (secondCurrLink = (*currLink)->inNode->getOutLinks().begin();
							secondCurrLink != (*currLink)->inNode->getOutLinks().end();
							++secondCurrLink)
						{
							if ((*currLink) == (*secondCurrLink))
							{
								(*currLink)->inNode->getOutLinks().erase(secondCurrLink);
								break;
							}
						}

						for (secondCurrLink = links.begin(); secondCurrLink != links.end(); ++secondCurrLink)
						{
							if ((*currLink) == (*secondCurrLink))
							{
								links.erase(secondCurrLink);
								break;
							}
						}
	
						delete (*currLink);
					}
	
					for (currLink = nodes[choice]->getOutLinks().begin(); currLink != nodes[choice]->getOutLinks().end(); ++currLink)
					{
						for (secondCurrLink = (*currLink)->outNode->getInLinks().begin();
							secondCurrLink != (*currLink)->outNode->getInLinks().end();
							++secondCurrLink)
						{
							if ((*currLink) == (*secondCurrLink))
							{
								(*currLink)->outNode->getInLinks().erase(secondCurrLink);
								break;
							}
						}
						
						for (secondCurrLink = links.begin(); secondCurrLink != links.end(); ++secondCurrLink)
						{
							if ((*currLink) == (*secondCurrLink))
							{
								links.erase(secondCurrLink);
								break;
							}
						}
						//Node deconstructor already calls delete on link
					}
	
					if (nodes[choice]->column == 0)
					{
						for (currNode = inputNodes.begin(); currNode != inputNodes.end(); ++currNode)
						{
							if (nodes[choice] == (*currNode))
							{
								inputNodes.erase(currNode);
								returnValue += 2;
								break;
							}
						}

					}
					else if (nodes[choice]->column == geneLength - 1)
					{
						for (currNode = outputNodes.begin(); currNode != outputNodes.end(); ++currNode)
						{
							if (nodes[choice] == (*currNode))
							{
								outputNodes.erase(currNode);
								returnValue += 2;
								break;
							}
						}

					}
	
					delete nodes[choice];
					nodes.erase(nodes.begin() + choice);
				}
			}
		}
	}*/

	//Creates a new Node in a random column in the gene
	if (percentageDistributor(randomGen) < newNodeMutation * ageModifier && nodes.size() < maxNodesInGene)
	{
		//std::uniform_int_distribution<int> columnPicker = std::uniform_int_distribution<int>(0, geneLength + geneLength - 2);
		std::vector<Node*>::iterator currNode;
	
		//This can pick an existing column, or the space between existing ones to create a new column
		//If it picked an existing column
		if (percentageDistributor(randomGen) >= newColumnMutation || geneLength > maxColumnsInAGene)
		{
			std::uniform_int_distribution<int> columnPicker = std::uniform_int_distribution<int>(0, geneLength - 1);
	
			int column = columnPicker(randomGen);
	
			//Node* node = new Node(column, offset);
			Node* node = new Node(column);

			if (column != 0)
			{
				makeRandomInLink(node);
			}
			
			if (column != geneLength - 1)
			{
				makeRandomOutLink(node);
			}
	
			nodes.push_back(node);
	
			int yOffset = 0;
	
			for (currNode = nodes.begin(); currNode != nodes.end(); ++currNode)
			{
				if ((*currNode)->column == node->column)
				{
					++yOffset;
				}
			}
	
			//node->shape.setPosition(node->shape.getPosition().x, offset.y + yOffset * lengthBetweenNodes);

			//If the node is placed at the input or output column of the gene
			if (column == 0)
			{
				inputNodes.push_back(node);
				addedInputOrOutputNodes = true;
				returnValue += 1;
			}
			else if (column == geneLength - 1)
			{
				outputNodes.push_back(node);
				addedInputOrOutputNodes = true;
				returnValue += 1;
			}
		}
		else
		{
			std::uniform_int_distribution<int> columnPicker = std::uniform_int_distribution<int>(0, geneLength - 2);
	
			//Location of the new column
			int columnSpace = columnPicker(randomGen) + 1;
	
			std::vector<Link*> linksToBeModified;
			std::vector<Link*>::iterator helperLinkIterator;
	
			for (currNode = nodes.begin(); currNode != nodes.end(); ++currNode)
			{
				if ((*currNode)->column >= columnSpace)
				{
					++((*currNode)->column);
				}
			}
	
			//Node* node = new Node(columnSpace, offset);
			Node* node = new Node(columnSpace);

			++geneLength;

			makeRandomInLink(node);
			makeRandomOutLink(node);

			nodes.push_back(node);
		}
	
	}

	++generation;

	return returnValue;
}

void Gene::activateGene()
{
	std::vector<Node*>::iterator currNode;
	for (int i = 0; i < geneLength; ++i)
	{
		for (currNode = nodes.begin(); currNode != nodes.end(); ++currNode)
		{
			if ((*currNode)->column == i)
			{
				(*currNode)->processInfo();
			}
		}
	}
}

/*void Gene::draw(sf::RenderWindow & window)
{
	std::vector<Node*>::iterator currNode;
	std::vector<Link*>::iterator currLink;

	for (currLink = links.begin(); currLink != links.end(); ++currLink)
	{
		(*currLink)->draw(window);
	}

	for (currNode = nodes.begin(); currNode != nodes.end(); ++currNode)
	{
		(*currNode)->draw(window);
	}
}*/

void Gene::makeRandomOutLink(Node * node)
{
	if (node->getOutLinks().size() < maxLinksOutOfNode)
	{
		std::uniform_int_distribution<int> columnPicker(0, geneLength - 1);

		std::vector<Node*> possibleNodes;
		nodes.reserve(nodes.size() - 1);

		std::vector<Node*>::iterator currNode;
		std::vector<Link*>::iterator currLink;

		int column;
		int tries = 0;

		do
		{
			column = node->column + columnPicker(randomGen) - columnPicker(randomGen);
			
			++tries;
		} while ((column < 0 || column >= geneLength) && tries < 10);

		if (tries == 10)
		{
			if (column < 0)
			{
				column = 0;
			}
			else if (column >= geneLength)
			{
				column = geneLength - 1;
			}
		}


		for (currNode = nodes.begin(); currNode != nodes.end(); ++currNode)
		{
			if ((*currNode)->column == column)
			{
				for (currLink = (*currNode)->getInLinks().begin(); currLink != (*currNode)->getInLinks().end(); ++currLink)
				{
					if ((*currLink)->inNode == (*currNode))
					{
						break;
					}
				}

				if (currLink == (*currNode)->getInLinks().end())
				{
					possibleNodes.push_back((*currNode));
				}
			}
		}

		if (possibleNodes.size() != 0)
		{
			std::uniform_int_distribution<int> nodePicker(0, possibleNodes.size() - 1);

			Link* link = new Link(node, possibleNodes[nodePicker(randomGen)]);

			links.push_back(link);
		}
		else
		{
			//printf("Couldn't find any nodes to make links with for some reason. Outlinks.\n");
		}
	}
	else
	{
		//printf("Too many connections from this node already.\n");
	}
}

void Gene::makeRandomInLink(Node * node)
{
	std::uniform_int_distribution<int> columnPicker(0, geneLength - 1);

	std::vector<Node*> possibleNodes;
	nodes.reserve(nodes.size() - 1);

	std::vector<Node*>::iterator currNode;
	std::vector<Link*>::iterator currLink;

	int column;
	int tries = 0;

	do
	{
		column = node->column + columnPicker(randomGen) - columnPicker(randomGen);

		++tries;
	} while ((column < 0 || column >= geneLength) && tries < 10);

	if (tries == 10)
	{
		if (column < 0)
		{
			column = 0;
		}
		else if (column >= geneLength)
		{
			column = geneLength - 1;
		}
	}


	for (currNode = nodes.begin(); currNode != nodes.end(); ++currNode)
	{
		if ((*currNode)->column == column)
		{
			for (currLink = (*currNode)->getOutLinks().begin(); currLink != (*currNode)->getOutLinks().end(); ++currLink)
			{
				if ((*currLink)->outNode == (*currNode))
				{
					break;
				}
			}

			if (currLink == (*currNode)->getOutLinks().end())
			{
				possibleNodes.push_back((*currNode));
			}
		}
	}

	if (possibleNodes.size() != 0)
	{
		std::uniform_int_distribution<int> nodePicker(0, possibleNodes.size() - 1);

		Link* link = new Link(possibleNodes[nodePicker(randomGen)], node);

		links.push_back(link);
	}
	else
	{
		//printf("Couldn't find any nodes to make links with for some reason. Inlinks.\n");
	}
}
