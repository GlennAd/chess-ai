#include "Link.h"

#include "Config.h"

Link::Link(Link & link)
{}

Link::Link(Node* inNode, Node* outNode)
{
	this->inNode = inNode;
	this->outNode = outNode;

	inNode->addOutputLink(this);
	outNode->addInputLink(this);

	std::uniform_int_distribution<int> randomThingy(-1, 1);

	weight = 1.0f * randomThingy(randomGen);
	value = 0.0f;

	//line = sf::RectangleShape(sf::Vector2f(1.0f, 1.0f));
	//line.setFillColor(sf::Color::Transparent);
}

Link::Link(Node * inNode, Node * outNode, float weight)
{
	this->inNode = inNode;
	this->outNode = outNode;

	inNode->addOutputLink(this);
	outNode->addInputLink(this);

	if (weight <= 1.0f && weight >= -1.0f)
	{
		this->weight = weight;
	}
	else
	{
		printf("Error: Weight was outside allowed values.\n");
		this->weight = 1.0f;
	}
	value = 0.0f;

	//line = sf::RectangleShape(sf::Vector2f(1.0f, 1.0f));
	//
	//line.setOutlineColor(sf::Color::Transparent);

}

void Link::mutate()
{
	float amount = (percentageDistributor(randomGen) - percentageDistributor(randomGen)) / 100.0f;

	weight += amount;

	if (weight > 1.0f)
	{
		weight = 1.0f;
	}
	else if (weight < -1.0f)
	{
		weight = -1.0f;
	}
}

/*void Link::draw(sf::RenderWindow & window)
{
	line.setPosition(inNode->shape.getPosition());

	sf::Vector2f diff = outNode->shape.getPosition() - inNode->shape.getPosition();

	float distance = sqrt(diff.x * diff.x + diff.y * diff.y);

	line.setSize(sf::Vector2f(distance, 5.0f));

	float angleInRad;
	
	if (diff.x >= 0)
	{
		angleInRad = asin(diff.y / distance);
	}
	else
	{
		angleInRad = asin(diff.y / distance) + ((3.14159f / 2.0f - asin(diff.y / distance)) * 2.0f);
	}

	line.setRotation(angleInRad * 180.0f / 3.14159265f);

	int weightColor = ((weight + 1.0f) / 2.0f) * 255;


	line.setFillColor(sf::Color(weightColor, weightColor, weightColor, 255));


	window.draw(line);
}*/
