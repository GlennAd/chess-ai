#include "ShaderHandler.h"

const std::string shaderPath = "./resources/shaders/";

ShaderHandler::ShaderProgram* ShaderHandler::initializeShaders()
{
	this->shaders["pointLight"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("pointLight", false));
	this->shaders["flashLight"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("flashLight", false));
	this->shaders["shadowShader"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("shadowShader", true));
	this->shaders["color"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("color", false));
	return this->shaders["pointLight"].get();
}


ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader, bool geometryShader)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";
	const std::string geometrySuffix = ".gs";
	if (geometryShader)
	{
		this->programId = LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str(), (shaderPath + shader + geometrySuffix).c_str());
	}
	else
	{
		this->programId = LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str());
	}

	//Finds uniform location for general info
	this->MVPId = glGetUniformLocation(this->programId, "MVP");
	this->modelMatrixId = glGetUniformLocation(this->programId, "modelMatrix");
	this->textureId = glGetUniformLocation(this->programId, "modelTexture");
	this->cameraPositionId = glGetUniformLocation(this->programId, "cameraPosition");
	this->color = glGetUniformLocation(this->programId, "diffuseColor");

	// Spotlight
	this->lightPositionId = glGetUniformLocation(this->programId, "light.position");
	this->lightConstantId = glGetUniformLocation(this->programId, "light.constant");
	this->lightLinearId = glGetUniformLocation(this->programId, "light.linear");
	this->lightQuadraticId = glGetUniformLocation(this->programId, "light.quadratic");
	this->lightDiffuseId = glGetUniformLocation(this->programId, "light.diffuse");
	this->lightSpecularId = glGetUniformLocation(this->programId, "light.specular");
	//Flashlight
	this->flashLightDirectionId = glGetUniformLocation(this->programId, "flashLight.direction");
	this->flashLightCutoffId = glGetUniformLocation(this->programId, "flashLight.cutOff");
	this->flashLightOuterCutoffId = glGetUniformLocation(this->programId, "flashLight.outerCutOff");

	//DepthMap
	this->farPlaneID = glGetUniformLocation(this->programId, "farPlane");
	this->depthMapID = glGetUniformLocation(this->programId, "depthMap");
	this->lighPosID = glGetUniformLocation(this->programId, "lightPos");

	this->pickingColorID = glGetUniformLocation(this->programId, "PickingColor");

	glUseProgram(this->programId);
}

ShaderHandler::ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(this->programId);
}

//----------------------------------------------GET--------------------------------------
ShaderHandler::ShaderProgram* ShaderHandler::getShader(const std::string& shader)
{
	auto it = this->shaders.find(shader);
	if (it != this->shaders.end())
	{
		return this->shaders[shader].get();
	}

	return nullptr;
}